package io.simplematter.streamliner.test.event;

public class OrderCreated implements Event {

    private String id;
    private String productId;
    private int quantity;
    private double price;
    private String customerId;
    private long createdAt;

    public OrderCreated(final String id, final String productId, final int quantity, final double price,
                        final String customerId, final long createdAt) {

        this.id = id;
        this.productId = productId;
        this.quantity = quantity;
        this.price = price;
        this.customerId = customerId;
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public String getProductId() {
        return productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public String getCustomerId() {
        return customerId;
    }

    public long getCreatedAt() {
        return createdAt;
    }
}
