package io.simplematter.streamliner.test.event;

public class CustomerConfirmed implements Event {

    private final String id;
    private final long createdAt;

    public CustomerConfirmed(final String id, final long createdAt) {

        this.id = id;
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public long getCreatedAt() {
        return createdAt;
    }
}
