package io.simplematter.streamliner.test.state;

import io.simplematter.streamliner.protocol.Offset;

public class Customer implements State {

    private String id;
    private String email;
    private String firstName;
    private String lastName;
    private String status;
    private Offset offset;
    private long createdAt;
    private long modifiedAt;

    public Customer(final String id, final String email, final String firstName, final String lastName,
                    final String status, final Offset offset, final long createdAt, final long modifiedAt) {

        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.status = status;
        this.offset = offset;
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getStatus() {
        return status;
    }

    public Offset getOffset() {
        return offset;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public long getModifiedAt() {
        return modifiedAt;
    }
}
