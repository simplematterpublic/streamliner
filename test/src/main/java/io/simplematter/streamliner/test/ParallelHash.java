package io.simplematter.streamliner.test;

import java.util.*;

public class ParallelHash {

    private final int parallelism;
    private Map<Integer, List<String>> hashes;

    public ParallelHash(int parallelism) {

        this.parallelism = parallelism;
        this.hashes = Collections.synchronizedMap(new HashMap<>());
        for (int i = 0; i < parallelism; i++) {
            hashes.put(i, Collections.synchronizedList(new ArrayList<>()));
        }
    }

    public void add(final List<String> records) {

        records.stream().forEach(record -> {

            final Random r = new Random();
            final int bucket = r.nextInt(5);
            final List<String> tasks = hashes.get(bucket);
            tasks.add(record);
            hashes.put(bucket, tasks);

        });
    }

    public void add(final String task) {

        final Random r = new Random();
        final int bucket = r.nextInt(5);
        final List<String> tasks = hashes.get(bucket);
        tasks.add(task);
        hashes.put(bucket, tasks);
    }

    private class Task {

        private Task() {

        }

    }

}
