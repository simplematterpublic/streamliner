package io.simplematter.streamliner.test;


import io.simplematter.streamliner.client.consumer.ConsumerByKey;
import io.simplematter.streamliner.protocol.messaging.ConsumerRecord;
import io.simplematter.streamliner.protocol.messaging.codec.Codec;

import java.util.List;
import java.util.Properties;

public class TestConsumer1 {

    public static void main(String[] args) throws Exception {

        final Properties props = new Properties();
        props.put("streamliner.config.server.name", "127.0.0.1");
        props.put("streamliner.config.server.port", 1234);
        props.put("streamliner.config.topic", "my_persistence_1");
        props.put("streamliner.config.clientId", "client1");
        props.put("streamliner.config.username", "stefano");
        props.put("streamliner.config.password", "rocco");
        props.put("streamliner.config.codec", Codec.class.getName());

        final ConsumerByKey<String, String> ConsumerByKey = new ConsumerByKey<String, String>(props,
                new StringSerdes(), new StringSerdes(), "key1");
        while (true) {
            final List<ConsumerRecord<String, String>> reads = ConsumerByKey.poll();
            if (reads.size() > 0) {
                System.out.println("Records: " + reads);
                /*
                List<ConsumerRecord<String, String>> keys = consumerById.lookup("key1", reads.get(reads.size() -1).getOffset());
                while (keys.size() == 0) {
                    keys = consumerById.lookup("key1", reads.get(reads.size() -1).getOffset());
                }
                if (keys.size() > 0) {
                    System.out.println(keys);
                } */
                //consumerById.acknowledge(reads.get(reads.size() -1).getOffset());
            }
            try {
                Thread.sleep(2000);
            } catch (Exception e) {

            }
        }
    }
}
