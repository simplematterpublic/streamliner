package io.simplematter.streamliner.test;

import io.simplematter.streamliner.protocol.serialization.Serdes;

public class StringSerdes<String> implements Serdes<String> {

    public StringSerdes() {

    }

    @Override
    public byte[] serialize(String value) {
        return new byte[0];
    }

    @Override
    public String deserialize(byte[] value) {
        return null;
    }
}
