package io.simplematter.streamliner.test.event;

public class CustomerCreated implements Event {

    private String id;
    private String email;
    private String firstName;
    private String lastName;
    private long createdAt;

    public CustomerCreated(final String id, final String email, final String firstName,
                           final String lastName, final long createdAt) {

        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public long getCreatedAt() {
        return createdAt;
    }
}
