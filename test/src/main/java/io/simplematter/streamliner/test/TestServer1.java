package io.simplematter.streamliner.test;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import io.simplematter.streamliner.server.Server;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;
import store.CassandraStoreFactory;

public class TestServer1 {

    public static void main(String[] args) {

        DeploymentOptions options = new DeploymentOptions().setInstances(Runtime.getRuntime().availableProcessors());
        Vertx vertx = Vertx.vertx();
        final Cluster cluster = Cluster.builder().addContactPoint("127.0.0.1").build();
        final Session session = cluster.connect("my_keyspace_1");
        final CassandraStoreFactory storeFactory = new CassandraStoreFactory(session);
        final Server server = new Server(new HazelcastClusterManager(), storeFactory, "127.0.0.1", 1234);
        vertx.deployVerticle(server);
    }

}
