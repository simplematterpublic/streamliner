package io.simplematter.streamliner.test.event;

public class OrderConfirmed implements Event {

    private String id;
    private long createdAt;

    private OrderConfirmed(final String id, final long createdAt) {

        this.id = id;
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public long getCreatedAt() {
        return createdAt;
    }
}
