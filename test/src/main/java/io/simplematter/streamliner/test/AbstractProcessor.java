package io.simplematter.streamliner.test;

import io.simplematter.streamliner.client.consumer.ConsumerByKey;
import io.simplematter.streamliner.protocol.Offset;
import io.simplematter.streamliner.protocol.messaging.ConsumerRecord;
import io.simplematter.streamliner.protocol.model.Tag;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AbstractProcessor<K, V> {

    private ConsumerByKey<K, V> consumerById;
    private ConsumerRecord<K, V> record;

    public AbstractProcessor(ConsumerByKey<K, V> consumerById) {

        this.consumerById = consumerById;
    }

    public void process(V value) {

    }

    private void acknowledge(final Offset offset) {

    }

    public Tag getPersistenceTag() {

        return record.getTag();
    }

    public K getKey() {

        return record.getKey();
    }

    public Offset getOffset() {

        return record.getOffset();
    }

    private void receive(ConsumerRecord<K, V> record) {

        this.record = record;
    }

    public class Poll {

        private List<ConsumerRecord<K, V>> records;

        public Poll(final ConsumerByKey<K, V> consumer) {

            this.records = Collections.synchronizedList(new ArrayList<ConsumerRecord<K, V>>());
        }

        public void accept(ConsumerRecord<K, V> record) {

            records.add(record);
        }

        public ConsumerRecord<K, V> next() {

            return records.remove(0);
        }

        public void run() {

            final List<ConsumerRecord<K, V>> records = consumerById.poll();
            records.stream().forEach(record -> {
                records.add(record);
            });
        }
    }

}
