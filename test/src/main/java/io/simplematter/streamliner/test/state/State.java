package io.simplematter.streamliner.test.state;

import java.io.Serializable;

public interface State extends Serializable {

    String getId();

    long getCreatedAt();

    long getModifiedAt();
}
