package io.simplematter.streamliner.test.state;

import io.simplematter.streamliner.protocol.Offset;

public class Order implements State {

    private String id;
    private String productId;
    private int quantity;
    private double price;
    private String customerId;
    private String notes;
    private String status;
    private Offset offset;
    private long createdAt;
    private long modifiedAt;

    public Order(final String id, final String productId, final int quantity, final double price,
                 final String customerId, final String notes, final String status, final Offset offset,
                 final long createdAt, final long modifiedAt) {

        this.id = id;
        this.productId = productId;
        this.quantity = quantity;
        this.price = price;
        this.customerId = customerId;
        this.notes = notes;
        this.status = status;
        this.offset = offset;
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }

    public String getId() {
        return id;
    }

    public String getProductId() {
        return productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getNotes() {
        return notes;
    }

    public String getStatus() {
        return status;
    }

    public Offset getOffset() {
        return offset;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public long getModifiedAt() {
        return modifiedAt;
    }
}
