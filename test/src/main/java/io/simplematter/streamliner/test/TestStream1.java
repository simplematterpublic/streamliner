package io.simplematter.streamliner.test;

import io.simplematter.streamliner.client.streaming.StreamByKey;
import io.simplematter.streamliner.protocol.messaging.ConsumerRecord;
import io.simplematter.streamliner.protocol.messaging.codec.Codec;

import java.util.Properties;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestStream1 {

    public static void main(String[] args) throws Exception {

        final Properties props = new Properties();
        props.put("streamliner.config.server.name", "127.0.0.1");
        props.put("streamliner.config.server.port", 1234);
        props.put("streamliner.config.topic", "my_persistence");
        props.put("streamliner.config.clientId", "client9");
        props.put("streamliner.config.username", "stefano");
        props.put("streamliner.config.password", "rocco");
        props.put("streamliner.config.codec", Codec.class.getName());

        final StreamByKey<String, String> streamById = new StreamByKey<>(props,
                new StringSerdes(), new StringSerdes(), "persistence1");
        System.out.println("START");
        Stream<ConsumerRecord<String, String>> stream1 = streamById.stream();

        final Stream<ConsumerRecord<String, String>> records = stream1.filter(new Predicate<ConsumerRecord<String, String>>() {
            @Override
            public boolean test(ConsumerRecord<String, String> stringStringConsumerRecord) {

                return true;
            }
        });


        System.out.println(" 1 " + records.limit(10).collect(Collectors.toList()));

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            System.out.println(e);
        }
        //stream1.close();


        //Stream<ConsumerRecord<String, String>> stream2 = streamById.stream();
        //System.out.println(" 2 " + stream2.limit(10).collect(Collectors.toList()));



        //System.out.println(" 2 " + stream2.limit(10).collect(Collectors.toList()));

        System.out.println("END");

        /*
        stream1.forEach(r -> {
            System.out.println(r);
        }); */

        /*
        System.out.println("Stream Retrieved");
        List<ConsumerRecord<String, String>> records = null;
        records = stream1.limit(1).collect(Collectors.toList());
        System.out.println("Records Collected 1");
        for (int i = 0; i < records.size(); i++) {
            System.out.println("Record 1 " + records.get(i));
            streamById.fetch(new Cursor()).forEach(r -> {
                System.out.println("Fetched " + r);
            });
            //streamById.acknowledge(records.get(i).getOffset());
        }
        System.out.println("Ended");
        */
        /*
        System.out.println("**********************************************************************");
        Stream<ConsumerRecord<String, String>> stream2 = streamById.stream();
        records = stream2.limit(10).collect(Collectors.toList());
        System.out.println("Records Collected 1");
        for (int i = 0; i < records.size(); i++) {
            System.out.println("Record 2 " + records.get(i));
            streamById.acknowledge(records.get(i).getOffset());
        } */

        /*
        records = stream.limit(10).collect(Collectors.toList());
        System.out.println("Records Collected 2");
        for (int i = 0; i < records.size(); i++) {
            System.out.println(records.get(i));
        } */
    }
}
