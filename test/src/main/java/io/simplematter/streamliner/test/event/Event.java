package io.simplematter.streamliner.test.event;

import java.io.Serializable;

public interface Event extends Serializable {

    String getId();

    long getCreatedAt();
}
