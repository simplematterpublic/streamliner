package io.simplematter.streamliner.test.event;

public class OrderRejected implements Event {

    private String id;
    private String reason;
    private long createdAt;

    private OrderRejected(final String id, final String reason, final long createdAt) {

        this.id = id;
        this.reason = reason;
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public String getReason() {
        return reason;
    }

    public long getCreatedAt() {
        return createdAt;
    }
}
