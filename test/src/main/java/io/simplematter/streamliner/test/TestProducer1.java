package io.simplematter.streamliner.test;

import io.simplematter.streamliner.client.producer.Producer;
import io.simplematter.streamliner.client.producer.core.IProducer;
import io.simplematter.streamliner.client.producer.core.Transaction;
import io.simplematter.streamliner.protocol.messaging.ProducerRecord;
import io.simplematter.streamliner.protocol.messaging.codec.Codec;
import io.simplematter.streamliner.protocol.model.Tag;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.UUID;

public class TestProducer1 {

    public static void main(String[] args) throws Exception {

        final Properties props = new Properties();
        props.put("streamliner.config.server.name", "127.0.0.1");
        props.put("streamliner.config.server.port", 1234);
        props.put("streamliner.config.topic", "my_persistence_1");
        props.put("streamliner.config.clientId", UUID.randomUUID().toString());
        props.put("streamliner.config.username", "stefano");
        props.put("streamliner.config.password", "rocco");
        props.put("streamliner.config.codec", Codec.class.getName());
        final IProducer<String, String> producer = new Producer<String, String>(props, new StringSerdes(), new StringSerdes());

        while (true) {
            final ArrayList<ProducerRecord<byte[], byte[]>> records = new ArrayList<ProducerRecord<byte[], byte[]>>();

            final Transaction<String, String> transaction = producer.transact();
            transaction.add(new ProducerRecord<String, String>(new Tag("event"), "key1", "value1"));
            transaction.add(new ProducerRecord<String, String>(new Tag("state"), "key1", "value1"));

            try {
                transaction.commit();
            } catch (IOException e) {
                System.out.println(e);
            }
            try {
                Thread.sleep(2000);
            } catch (Exception e) {

            }
        }
    }
}
