package io.simplematter.streamliner.protocol.messaging.response;

import io.simplematter.streamliner.protocol.Offset;

public class FetchCursorResponse extends AbstractResponse {

    private final Offset offset;

    public FetchCursorResponse(final ResponseHeader header, final Offset offset) {

        super(header);
        this.offset = offset;
    }

    public Offset getOffset() {

        return offset;
    }

    @Override
    public String toString() {

        final StringBuilder builder = new StringBuilder();
        builder.append("{");
        builder.append(getClass().getSimpleName());
        builder.append(":{header:");
        builder.append(getHeader().toString());
        builder.append(",offset:");
        builder.append(getOffset().toString());
        builder.append("}}");
        return builder.toString();
    }
}
