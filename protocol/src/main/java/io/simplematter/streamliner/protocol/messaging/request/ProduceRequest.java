package io.simplematter.streamliner.protocol.messaging.request;

import io.simplematter.streamliner.protocol.messaging.ProducerRecord;

import java.util.List;

public class ProduceRequest extends AbstractRequest {

    private final List<ProducerRecord<byte[], byte[]>> records;

    public ProduceRequest(final RequestHeader header, final String topic, final List<ProducerRecord<byte[], byte[]>> records) {

        super(header, topic);
        this.records = records;
    }

    public List<ProducerRecord<byte[], byte[]>> getRecords() {

        return records;
    }

    @Override
    public String toString() {

        final StringBuilder builder = new StringBuilder();
        builder.append("{");
        builder.append(getClass().getSimpleName());
        builder.append(":{header:");
        builder.append(getHeader().toString());
        builder.append(",records:[");
        builder.append(getRecords().size());
        builder.append("]}}");
        return builder.toString();
    }
}
