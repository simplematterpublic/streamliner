package io.simplematter.streamliner.protocol.messaging.response;

import io.simplematter.streamliner.protocol.messaging.ConsumerRecord;

import java.util.List;

public class FetchByKeyResponse extends AbstractResponse {

    private final List<ConsumerRecord<byte[], byte[]>> records;

    public FetchByKeyResponse(final ResponseHeader header, final List<ConsumerRecord<byte[], byte[]>> records) {

        super(header);
        this.records = records;
    }

    public List<ConsumerRecord<byte[], byte[]>> getRecords() {

        return records;
    }

    @Override
    public String toString() {

        final StringBuilder builder = new StringBuilder();
        builder.append("{");
        builder.append(getClass().getSimpleName());
        builder.append(":{header:");
        builder.append(getHeader().toString());
        builder.append(",records:[");
        builder.append(getRecords().size());
        builder.append("]}}");
        return builder.toString();
    }
}
