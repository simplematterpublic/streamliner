package io.simplematter.streamliner.protocol.messaging.response;

public class LeadershipNotification extends AbstractResponse {

    private final boolean isLeader;

    public LeadershipNotification(final ResponseHeader header, final boolean isLeader) {
        super(header);
        this.isLeader = isLeader;
    }

    public boolean isLeader() {

        return isLeader;
    }

    @Override
    public String toString() {

        final StringBuilder builder = new StringBuilder();
        builder.append("{");
        builder.append(getClass().getSimpleName());
        builder.append(":{header:");
        builder.append(getHeader().toString());
        builder.append(",isLeader:");
        builder.append(isLeader());
        builder.append("}}");
        return builder.toString();
    }

}
