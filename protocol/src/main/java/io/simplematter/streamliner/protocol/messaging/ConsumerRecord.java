/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.simplematter.streamliner.protocol.messaging;

import io.simplematter.streamliner.protocol.Offset;
import io.simplematter.streamliner.protocol.Record;
import io.simplematter.streamliner.protocol.model.Tag;

public class ConsumerRecord<K, V> extends Record<K, V> {

    private final String uid;
    private final Offset offset;
    private final long timestamp;

    public ConsumerRecord(final String uid, final Tag tag, final K key, final V value, final Offset offset, final long timestamp) {
        super(tag, key, value);
        this.uid = uid;
        this.offset = offset;
        this.timestamp = timestamp;
    }

    public ConsumerRecord(final String uid, final Tag tag, final K key, final V value, final Offset offset) {
        super(tag, key, value);
        this.uid = uid;
        this.offset = offset;
        this.timestamp = System.currentTimeMillis();
    }

    public String getUid() {
        return uid;
    }

    public Offset getOffset() {
        return  offset;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {

        final StringBuilder builder = new StringBuilder();
        builder.append("{");
        builder.append(getClass().getSimpleName());
        builder.append("{uid:");
        builder.append(getUid());
        builder.append(",tag:");
        builder.append(getTag());
        builder.append(",key:");
        builder.append(getKey());
        builder.append(",value:");
        builder.append(getValue());
        builder.append(",offset:");
        builder.append(getOffset());
        builder.append(",timestamp:");
        builder.append(getTimestamp());
        builder.append("}");
        return builder.toString();
    }
}
