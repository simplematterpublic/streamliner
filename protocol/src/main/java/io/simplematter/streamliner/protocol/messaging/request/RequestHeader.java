package io.simplematter.streamliner.protocol.messaging.request;

import java.io.Serializable;

public class RequestHeader implements Serializable {

    private final int apiKey;
    private final int apiVersion;
    private final long correlationId;
    private final String clientId;
    private final long timestamp;

    public RequestHeader(final int apiKey, final int apiVersion, final long correlationId, final String clientId, final long timestamp) {

        this.apiKey = apiKey;
        this.apiVersion = apiVersion;
        this.correlationId = correlationId;
        this.clientId = clientId;
        this.timestamp = timestamp;
    }

    public RequestHeader(final int apiKey, final int apiVersion, final long correlationId, final String clientId) {

        this(apiKey, apiVersion, correlationId, clientId, System.currentTimeMillis());
    }

    public int getApiKey() {
        return apiKey;
    }

    public int getApiVersion() {
        return apiVersion;
    }

    public long getCorrelationId() {
        return correlationId;
    }

    public String getClientId() {
        return clientId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {

        final StringBuilder builder = new StringBuilder();
        builder.append("{apiKey:");
        builder.append(getApiKey());
        builder.append(",apiVersion:");
        builder.append(getApiVersion());
        builder.append(",correlationId:");
        builder.append(getCorrelationId());
        builder.append(",clientId:");
        builder.append(getClientId());
        builder.append(",timestamp:");
        builder.append(getTimestamp());
        builder.append("}");
        return builder.toString();
    }
}
