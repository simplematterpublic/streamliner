package io.simplematter.streamliner.protocol.model;

import java.io.Serializable;

public interface Value extends Serializable {

    String getValue();
}
