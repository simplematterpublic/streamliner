package io.simplematter.streamliner.protocol.messaging.request;

public class FetchCursorRequest extends AbstractRequest {

    public FetchCursorRequest(final RequestHeader header, final String topic) {

        super(header, topic);
    }

    @Override
    public String toString() {

        final StringBuilder builder = new StringBuilder();
        builder.append("{");
        builder.append(getClass().getSimpleName());
        builder.append(":{header:");
        builder.append(getHeader().toString());
        builder.append("}}");
        return builder.toString();
    }

}
