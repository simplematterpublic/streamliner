package io.simplematter.streamliner.protocol.messaging.request;

import java.io.Serializable;

public class AbstractRequest implements Serializable {

    private final RequestHeader header;
    private String topic;

    public AbstractRequest(final RequestHeader header, final String topic) {

        this.header = header;
        this.topic = topic;
    }

    public RequestHeader getHeader() {
        return header;
    }

    public String getClientId() {

        return header.getClientId();
    }

    public long getCorrelationId() {

        return header.getCorrelationId();
    }

    public String getTopic() {

        return topic;
    }
}
