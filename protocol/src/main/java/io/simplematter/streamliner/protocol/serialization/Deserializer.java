package io.simplematter.streamliner.protocol.serialization;

public interface Deserializer<T> {

    T deserialize(byte[] value);
}
