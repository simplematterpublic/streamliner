package io.simplematter.streamliner.protocol.messaging.response;

import java.io.Serializable;

public class AbstractResponse implements Serializable {

    private final ResponseHeader header;

    public AbstractResponse(final ResponseHeader header) {

        this.header = header;
    }

    public ResponseHeader getHeader() {
        return header;
    }
}
