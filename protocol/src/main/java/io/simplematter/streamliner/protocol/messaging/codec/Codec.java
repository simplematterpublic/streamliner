package io.simplematter.streamliner.protocol.messaging.codec;

import java.io.*;

public class Codec {

    public byte[] encode(final Object value) {

        return toBytes(value);
        /*
        if (value instanceof FetchByIdRequest) {
            return toBytes(value);
        } else if (value instanceof FetchByTagRequest) {
            return toBytes(value);
        } else {
            throw new RuntimeException("Encoding not supported " + value);
        } */
    }

    public Object decode(final byte[] value) {

        return fromBytes(value);
    }

    private byte[] toBytes(final Object value) {

        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            final ObjectOutput output = new ObjectOutputStream(bos);
            output.writeObject(value);
            output.flush();
            return bos.toByteArray();
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex.getMessage());
        } finally {
            try {
                bos.close();
            } catch (IOException ex) {
                ex.printStackTrace();
                // ignore close exception
            }
        }
    }

    private Object fromBytes(byte[] value) {

        final ByteArrayInputStream bis = new ByteArrayInputStream(value);
        ObjectInput in = null;
        try {
            in = new ObjectInputStream(bis);
            return in.readObject();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex.getMessage());
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex.getMessage());
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                // ignore close exception
            }
        }
    }
}
