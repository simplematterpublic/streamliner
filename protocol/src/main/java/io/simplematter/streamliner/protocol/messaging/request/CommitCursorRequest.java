package io.simplematter.streamliner.protocol.messaging.request;

import io.simplematter.streamliner.protocol.Offset;

public class CommitCursorRequest extends AbstractRequest {

    private final Offset offset;

    public CommitCursorRequest(final RequestHeader header, final String topic, final Offset offset) {

        super(header, topic);
        this.offset = offset;
    }

    public Offset getOffset() {

        return offset;
    }

    @Override
    public String toString() {

        final StringBuilder builder = new StringBuilder();
        builder.append("{");
        builder.append(getClass().getSimpleName());
        builder.append(":{header:");
        builder.append(getHeader().toString());
        builder.append(",offset:");
        builder.append(getOffset().toString());
        builder.append("}}");
        return builder.toString();
    }
}
