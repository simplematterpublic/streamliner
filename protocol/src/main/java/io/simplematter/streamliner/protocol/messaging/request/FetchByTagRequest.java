package io.simplematter.streamliner.protocol.messaging.request;

import io.simplematter.streamliner.protocol.Offset;

public class FetchByTagRequest extends AbstractRequest {

    private final String tag;
    private final Offset offset;
    private final int batchSize;

    public FetchByTagRequest(final RequestHeader header, final String topic, final String tag,
                             final Offset offset, final int batchSize) {

        super(header, topic);
        this.tag = tag;
        this.offset = offset;
        this.batchSize = batchSize;
    }

    public String getTag() {
        return tag;
    }

    public Offset getOffset() {
        return offset;
    }

    public int getBatchSize() {
        return batchSize;
    }

    @Override
    public String toString() {

        final StringBuilder builder = new StringBuilder();
        builder.append("{");
        builder.append(getClass().getSimpleName());
        builder.append(":{header:");
        builder.append(getHeader().toString());
        builder.append(",tag:");
        builder.append(getTag());
        builder.append(",offset:");
        builder.append(getOffset().toString());
        builder.append(",batchSize:");
        builder.append(getBatchSize());
        builder.append("}}");
        return builder.toString();
    }

}