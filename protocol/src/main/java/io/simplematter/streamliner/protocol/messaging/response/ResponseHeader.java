package io.simplematter.streamliner.protocol.messaging.response;

import java.io.Serializable;

public class ResponseHeader implements Serializable {

    private final long correlationId;
    private final long timestamp;

    public ResponseHeader(final long correlationId, final long timestamp) {

        this.correlationId = correlationId;
        this.timestamp = timestamp;
    }

    public ResponseHeader(final long correlationId) {

        this(correlationId, System.currentTimeMillis());
    }

    public long getCorrelationId() {
        return correlationId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {

        final StringBuilder builder = new StringBuilder();
        builder.append("{correlationId:");
        builder.append(getCorrelationId());
        builder.append(",timestamp:");
        builder.append(getTimestamp());
        builder.append("}");
        return builder.toString();
    }

}
