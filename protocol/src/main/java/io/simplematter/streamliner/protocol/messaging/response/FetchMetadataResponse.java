package io.simplematter.streamliner.protocol.messaging.response;

public class FetchMetadataResponse extends AbstractResponse {

    public FetchMetadataResponse(final ResponseHeader header) {

        super(header);
    }

}
