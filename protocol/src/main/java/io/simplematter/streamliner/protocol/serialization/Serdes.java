package io.simplematter.streamliner.protocol.serialization;

public interface Serdes<T> extends Serializer<T>, Deserializer<T> {

}
