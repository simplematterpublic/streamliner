package io.simplematter.streamliner.protocol.messaging.request;

public class FetchMetadataRequest extends AbstractRequest {

    public FetchMetadataRequest(final RequestHeader header, final String topic) {

        super(header, topic);
    }
}
