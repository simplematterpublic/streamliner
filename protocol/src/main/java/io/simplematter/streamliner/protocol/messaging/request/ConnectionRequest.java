package io.simplematter.streamliner.protocol.messaging.request;

public class ConnectionRequest extends AbstractRequest {

    private final String username;
    private final String password;


    public ConnectionRequest(final RequestHeader header, final String topic, final String username, final String password) {

        super(header, topic);
        this.username = username;
        this.password = password;
    }

    public String getUsername() {

        return username;
    }

    public String getPassword() {

        return password;
    }

    @Override
    public String toString() {

        final StringBuilder builder = new StringBuilder();
        builder.append("{");
        builder.append(getClass().getSimpleName());
        builder.append(":{header:");
        builder.append(getHeader().toString());
        builder.append(",topic:");
        builder.append(getTopic());
        builder.append(",username:");
        builder.append(getUsername());
        builder.append(",password:");
        // TODO: Encrypt
        builder.append(getPassword());
        builder.append("}}");
        return builder.toString();
    }
}
