package io.simplematter.streamliner.protocol.messaging.request;

import io.simplematter.streamliner.protocol.Offset;

public class FetchByKeyRequest extends AbstractRequest {

    private final byte[] key;
    private final Offset offset;
    private final int batchSize;

    public FetchByKeyRequest(final RequestHeader header, final String topic, final byte[] key,
                             final Offset offset, final int batchSize) {

        super(header, topic);
        this.key = key;
        this.offset = offset;
        this.batchSize = batchSize;
    }

    public byte[] getKey() {
        return key;
    }

    public Offset getOffset() {
        return offset;
    }

    public int getBatchSize() {
        return batchSize;
    }

    @Override
    public String toString() {

        final StringBuilder builder = new StringBuilder();
        builder.append("{");
        builder.append(getClass().getSimpleName());
        builder.append(":{header:");
        builder.append(getHeader().toString());
        builder.append(",key:");
        builder.append(getKey());
        builder.append(",offset:");
        builder.append(getOffset().toString());
        builder.append(",batchSize:");
        builder.append(getBatchSize());
        builder.append("}}");
        return builder.toString();
    }
}
