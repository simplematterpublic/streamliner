package io.simplematter.streamliner.protocol.serialization;

public interface Serializer<T> {

    byte[] serialize(T value);
}
