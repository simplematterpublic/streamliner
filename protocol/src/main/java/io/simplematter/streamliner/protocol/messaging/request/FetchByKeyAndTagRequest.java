package io.simplematter.streamliner.protocol.messaging.request;

import io.simplematter.streamliner.protocol.Offset;

public class FetchByKeyAndTagRequest extends AbstractRequest {

    private byte[] key;
    private String tag;
    private Offset offset;

    public FetchByKeyAndTagRequest(final RequestHeader header, final String topic, final byte[] key,
                                   final String tag, final Offset offset) {

        super(header, topic);
        this.key = key;
        this.tag = tag;
        this.offset = offset;
    }

    public byte[] getKey() {

        return key;
    }

    public String getTag() {

        return tag;
    }

    public Offset getOffset() {

        return offset;
    }

    @Override
    public String toString() {

        final StringBuilder builder = new StringBuilder();
        builder.append("{");
        builder.append(getClass().getSimpleName());
        builder.append(":{header:");
        builder.append(getHeader().toString());
        builder.append(",key:");
        builder.append(getKey());
        builder.append(",tag:");
        builder.append(getTag());
        builder.append(",offset:");
        builder.append(getOffset().toString());
        builder.append("}}");
        return builder.toString();
    }

}
