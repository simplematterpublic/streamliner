package io.simplematter.streamliner.protocol.messaging.response;

public class ConnectionResponse extends AbstractResponse {

    private final boolean isSuccessful;

    public ConnectionResponse(final ResponseHeader header, final boolean isSuccessful) {

        super(header);
        this.isSuccessful = isSuccessful;
    }

    public boolean isSuccessful() {

        return isSuccessful;
    }

    @Override
    public String toString() {

        final StringBuilder builder = new StringBuilder();
        builder.append("{");
        builder.append(getClass().getSimpleName());
        builder.append(":{header:");
        builder.append(getHeader().toString());
        builder.append(",isSuccessful:");
        builder.append(isSuccessful());
        builder.append("}}");
        return builder.toString();
    }

}
