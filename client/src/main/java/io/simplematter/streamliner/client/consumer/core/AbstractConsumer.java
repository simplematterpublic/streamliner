/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.simplematter.streamliner.client.consumer.core;

import io.simplematter.streamliner.client.NetworkClient;
import io.simplematter.streamliner.protocol.Offset;
import io.simplematter.streamliner.protocol.messaging.ConsumerRecord;
import io.simplematter.streamliner.protocol.model.Tag;
import io.simplematter.streamliner.protocol.serialization.Serdes;
import io.simplematter.streamliner.store.Cursor;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public abstract class AbstractConsumer<K, V> implements IConsumer<K, V> {

    private final NetworkClient client;
    private final Serdes<K> keySerdes;
    private final Serdes<V> valueSerdes;
    private AtomicBoolean isLeader;
    private Offset cursor;

    protected AbstractConsumer(final Properties props, Serdes<K> keySerdes, Serdes<V> valueSerdes) throws Exception {

        this.client = new NetworkClient(props);
        this.keySerdes = keySerdes;
        this.valueSerdes = valueSerdes;
        this.isLeader = new AtomicBoolean(false);
        this.cursor = new Cursor();
    }

    protected NetworkClient getClient() {

        return client;
    }

    protected Serdes<K> getKeySerdes() {

        return keySerdes;
    }

    protected Serdes<V> getValueSerdes() {

        return valueSerdes;
    }

    protected boolean isLeader() {

        if (getClient().isLeader()) {
            if (!isLeader.get()) {
                isLeader.set(true);
                setCursor(getClient().fetchCursor().getOffset());
            }
        } else {
            if (isLeader.get()) {
                isLeader.set(false);
            }
        }
        return isLeader.get();
    }

    @Override
    public Offset getOffset() {

        return getClient().fetchOffset().getOffset();
    }

    protected void setCursor(final Offset cursor) {

        synchronized (this) {
            if (cursor.compareTo(this.cursor) > 0) {
                this.cursor = cursor;
            }
        }
    }

    @Override
    public Offset getCursor() {

        return cursor;
    }

    @Override
    public boolean acknowledge(final Offset offset) {

        if (isLeader()) {
            // TODO: Check
            //if (offset.compareTo(getCursor()) > 0) {
                if (getClient().commitCursor(offset).isSuccessful()) {
                    setCursor(offset);
                    return true;
                } else {
                    return false;
                }
            //} else {
            //    return false;
            //}
        } else {
            return false;
        }
    }

    @Override
    public boolean reset(final Offset offset) {

        if (isLeader()) {
            if (getClient().resetCursor(offset).isSuccessful()) {
                setCursor(offset);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public boolean reset() {

        return reset(new Cursor());
    }

    @Override
    public List<ConsumerRecord<K, V>> fetch(final K key, final Tag tag, final Offset offset) {

        final List<ConsumerRecord<byte[], byte[]>> records = getClient()
                .fetchByKeyAndTag(keySerdes.serialize(key), tag, offset).getRecords();
        return deserialize(records);
    }

    protected List<ConsumerRecord<K, V>> deserialize(List<ConsumerRecord<byte[], byte[]>> records) {

        return records.stream()
                .map(r -> new ConsumerRecord<K, V>(r.getUid(),
                        r.getTag(),
                        keySerdes.deserialize(r.getKey()),
                        valueSerdes.deserialize(r.getValue()),
                        r.getOffset(),
                        r.getTimestamp()))
                .collect(Collectors.toList());
    }

    @Override
    public void close() {

        getClient().close();
    }
}
