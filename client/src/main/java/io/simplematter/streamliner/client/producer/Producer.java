/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.simplematter.streamliner.client.producer;

import io.simplematter.streamliner.client.NetworkClient;
import io.simplematter.streamliner.client.producer.core.IProducer;
import io.simplematter.streamliner.client.producer.core.Transaction;
import io.simplematter.streamliner.protocol.Offset;
import io.simplematter.streamliner.protocol.messaging.ProducerRecord;
import io.simplematter.streamliner.protocol.model.Tag;
import io.simplematter.streamliner.protocol.serialization.Serdes;

import java.util.List;
import java.util.Properties;
import java.util.UUID;

public class Producer<K, V> implements IProducer<K, V> {

    private final NetworkClient client;
    private final Serdes<K> keySerdes;
    private final Serdes<V> valueSerdes;

    public Producer(final Properties props, final Serdes<K> keySerdes, final Serdes<V> valueSerdes) throws Exception {

        props.setProperty("clientId", UUID.randomUUID().toString());
        this.client = new NetworkClient(props);
        this.keySerdes = keySerdes;
        this.valueSerdes = valueSerdes;
    }

    protected NetworkClient getClient() {

        return client;
    }

    @Override
    public Offset getOffset() {

        return getClient().fetchOffset().getOffset();
    }

    @Override
    public Transaction<K, V> transact() {
        return new Transaction(client, keySerdes, valueSerdes);
    }

    @Override
    public Transaction<K, V> produce(final Tag tag, final K key, final V value) {

        return new Transaction(client, keySerdes, valueSerdes).add(tag, key, value);
    }

    @Override
    public Transaction<K, V> produce(final ProducerRecord<K, V> record) {
        return new Transaction(client, keySerdes, valueSerdes).add(record);
    }

    @Override
    public Transaction<K, V> produce(final List<ProducerRecord<K, V>> records) {
        return new Transaction(client, keySerdes, valueSerdes).add(records);
    }

    @Override
    public Transaction<K, V> produce(final ProducerRecord<K, V>... records) {
        return new Transaction(client, keySerdes, valueSerdes).add(records);
    }
}
