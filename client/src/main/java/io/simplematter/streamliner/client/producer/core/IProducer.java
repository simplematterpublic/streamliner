/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.simplematter.streamliner.client.producer.core;

import io.simplematter.streamliner.protocol.Offset;
import io.simplematter.streamliner.protocol.messaging.ProducerRecord;
import io.simplematter.streamliner.protocol.model.Tag;

import java.util.List;

public interface IProducer<K, V> {

    Offset getOffset();

    Transaction<K, V> transact();

    Transaction<K, V> produce(Tag tag, K key, V value);

    Transaction<K, V> produce(ProducerRecord<K, V> record);

    Transaction<K, V> produce(List<ProducerRecord<K, V>> records);

    Transaction<K, V> produce(ProducerRecord<K, V>... records);
}
