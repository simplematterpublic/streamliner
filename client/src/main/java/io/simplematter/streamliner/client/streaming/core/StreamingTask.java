/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.simplematter.streamliner.client.streaming.core;

import io.simplematter.streamliner.client.consumer.core.IConsumer;
import io.simplematter.streamliner.protocol.messaging.ConsumerRecord;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;

public class StreamingTask<K, V> implements Runnable {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final ExecutorService executor;
    private final IConsumer<K, V> consumer;
    private final StreamingQueue queue;
    private final String id;
    private boolean isRunning;

    public StreamingTask(final ExecutorService executor, final IConsumer<K, V> consumer, final StreamingQueue queue) {

        this.executor = executor;
        this.consumer = consumer;
        this.queue = queue;
        this.id = UUID.randomUUID().toString();
    }

    private IConsumer<K, V> getConsumer() {

        return consumer;
    }

    public StreamingQueue getQueue() {

        return queue;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void start() {

        if (!isRunning) {
            logger.info("Stream starting: " + id);
            isRunning = true;
            executor.submit(this);
        } else {
            throw new RuntimeException("ConsumerTask is already running");
        }
    }

    public void stop() {

        logger.info("Stream stopping: " + id);
        //consumer.reset();
        //isRunning = false;
    }

    @Override
    public void run() {

        logger.info("Stream started: " + id);
        while (isRunning) {
            //System.out.println("Stream Running " + id);
            if (queue.size() < queue.maxLength()) {
                logger.info("Fetching new data");
                final List<ConsumerRecord<K, V>> records = consumer.poll();
                records.forEach(record -> queue.accept(record));
                if (records.size() == 0) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        // Ignore
                    }
                }
            }
            Thread.yield();
        }
        logger.info("Stream stopped: " + id);
    }
}
