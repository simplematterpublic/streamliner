/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.simplematter.streamliner.client.producer.core;

import io.simplematter.streamliner.client.NetworkClient;
import io.simplematter.streamliner.protocol.messaging.ProducerRecord;
import io.simplematter.streamliner.protocol.model.Tag;
import io.simplematter.streamliner.protocol.serialization.Serdes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Transaction<K, V> {

    private final NetworkClient client;
    private final Serdes<K> keySerdes;
    private final Serdes<V> valueSerdes;
    private final List<ProducerRecord<K, V>> records;
    private boolean isActive = true;

    public Transaction(final NetworkClient client,
                       final Serdes<K> keySerdes,
                       final Serdes<V> valueSerdes) {

        this.client = client;
        this.keySerdes = keySerdes;
        this.valueSerdes = valueSerdes;
        this.records = new ArrayList<>();
    }

    public Transaction<K, V> add(final ProducerRecord<K, V> record) {

        records.add(record);
        return this;
    }

    public Transaction<K, V> add(final Tag tag, final K key, final V value) {

        final ProducerRecord<K, V> record = new ProducerRecord(tag, key, value);
        return add(record);
    }

    public Transaction<K, V> add(final List<ProducerRecord<K, V>> records) {

        this.records.addAll(records);
        return this;
    }

    public Transaction<K, V> add(final ProducerRecord<K, V>... records) {

        return add(Arrays.asList(records));
    }

    public void commit() throws IOException {

        if (isActive) {
            isActive = false;
            final List<ProducerRecord<byte[], byte[]>> filtered = records.stream().map(record -> {
               if (record.getTag() == null) {
                   // TODO: FIXME
                   return new ProducerRecord<byte[], byte[]>(new Tag("none"),
                           keySerdes.serialize(record.getKey()),
                           valueSerdes.serialize(record.getValue()));
               } else {
                   return new ProducerRecord<byte[], byte[]>(record.getTag(),
                           keySerdes.serialize(record.getKey()),
                           valueSerdes.serialize(record.getValue()));
               }
            }).collect(Collectors.toList());
            client.produce(filtered);
        } else {
            throw new IOException("Cannot commit. Transaction is not active.");
        }
    }
}
