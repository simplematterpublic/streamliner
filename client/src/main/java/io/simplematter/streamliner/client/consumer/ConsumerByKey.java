/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.simplematter.streamliner.client.consumer;

import io.simplematter.streamliner.client.consumer.core.AbstractConsumer;
import io.simplematter.streamliner.protocol.Offset;
import io.simplematter.streamliner.protocol.messaging.ConsumerRecord;
import io.simplematter.streamliner.protocol.serialization.Serdes;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public final class ConsumerByKey<K, V> extends AbstractConsumer<K, V> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final K key;

    public ConsumerByKey(final Properties props,
                         final Serdes<K> keySerdes,
                         final Serdes<V> valueSerdes,
                         final K key) throws Exception {

        super(props, keySerdes, valueSerdes);
        this.key = key;
    }

    @Override
    public List<ConsumerRecord<K, V>> poll() {

        if (isLeader()) {
            System.out.println(key);
            final List<ConsumerRecord<byte[], byte[]>> records = getClient()
                    .fetchByKey(getKeySerdes().serialize(key), getCursor()).getRecords();
            if (records.size() > 0) {
                setCursor(records.get(records.size() - 1).getOffset());
            }
            return deserialize(records);
        } else {
            logger.info("Not a leader");
            return new ArrayList<>();
        }
    }

    @Override
    public List<ConsumerRecord<K, V>> fetch(final Offset offset) {

        final List<ConsumerRecord<byte[], byte[]>> all = new ArrayList<>();
        boolean fetch = true;
        while (fetch) {
            final List<ConsumerRecord<byte[], byte[]>> records = getClient()
                    .fetchByKey(getKeySerdes().serialize(key), offset).getRecords();
            if (records.size() > 0) {
                all.addAll(records);
            } else {
                fetch = false;
            }
        }
        return deserialize(all);
    }
}