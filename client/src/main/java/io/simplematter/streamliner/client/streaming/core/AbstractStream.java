/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.simplematter.streamliner.client.streaming.core;

import io.simplematter.streamliner.client.consumer.core.IConsumer;
import io.simplematter.streamliner.protocol.Offset;
import io.simplematter.streamliner.protocol.messaging.ConsumerRecord;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.stream.Stream;

public class AbstractStream<K, V> {

    private final ExecutorService executor;
    private Map<String, Stream<ConsumerRecord<K, V>>> streams;
    private final IConsumer<K, V> consumer;
    private InfiniteStream<K, V> stream;

    protected AbstractStream(final ExecutorService executor, final IConsumer<K, V> consumer) {

        this.streams = new HashMap<>();
        this.executor = executor;
        this.consumer = consumer;
    }

    protected IConsumer<K, V> getConsumer() {

        return consumer;
    }

    public Stream<ConsumerRecord<K, V>> stream() {
        if (stream == null || stream.isClosed()) {
            System.out.println("Creating new Stream");
            final StreamingTask task = new StreamingTask(executor, consumer, new StreamingQueue(1000));
            final InfiniteStream<K, V> stream = new InfiniteStream(task);
            return stream;
        } else {
            throw new RuntimeException("Stream already created");
        }
    }

    public List<ConsumerRecord<K, V>> fetch(final Offset offset) {

        return consumer.fetch(offset);
    }

    /*
    public List<ConsumerRecord<K, V>> lookup(final K key, final Offset offset) {

        return consumer.lookup(key, offset);
    } */

    public void close() {

        executor.shutdown();
        while (!executor.isTerminated()) {
            Thread.yield();
        }
        stream.close();
        stream = null;
    }

    public boolean acknowledge(final Offset offset) {

        return consumer.acknowledge(offset);
    }

    /*
    public Stream<ConsumerRecord<K, V>> rewind(final Offset offset) {

        if (consumer.reset(offset)) {
            isStarted = true;
            stream.close();
            stream = new InfiniteStream(new StreamingTask(consumer, new StreamingQueue(1000)));
            return stream;
        } else {
            throw new RuntimeException("Cannot rewind stream");
        }
    }

    public Stream<ConsumerRecord<K, V>> rewind() {

        return rewind(new Cursor());
    } */
}
