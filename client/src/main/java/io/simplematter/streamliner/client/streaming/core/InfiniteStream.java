package io.simplematter.streamliner.client.streaming.core;

import io.simplematter.streamliner.protocol.messaging.ConsumerRecord;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Optional;
import java.util.Spliterator;
import java.util.function.*;
import java.util.stream.*;

public class InfiniteStream<K, V> implements Stream<ConsumerRecord<K, V>>, AutoCloseable {

    private final Stream<ConsumerRecord<K, V>> stream;
    private final StreamingTask<K, V> task;
    private boolean isClosed;

    public InfiniteStream(final StreamingTask<K ,V> task) {

        this.task = task;
        this.stream = Stream.generate(task.getQueue());
        this.stream.onClose(new Runnable() {
            @Override
            public void run() {
                task.stop();
            }
        });
        this.task.start();
        isClosed = false;
    }

    /*
    @Override
    public void accept(T t) {
        q.accept(t);
    }
    */

    @Override
    public Iterator<ConsumerRecord<K, V>> iterator() {

        return stream.iterator();
    }

    @Override
    public Spliterator<ConsumerRecord<K, V>> spliterator() {

        return stream.spliterator();
    }

    @Override
    public boolean isParallel() {
        return stream.isParallel();
    }

    @Override
    public Stream<ConsumerRecord<K, V>> sequential() {
        return stream.sequential();
    }

    @Override
    public Stream<ConsumerRecord<K, V>> parallel() {
        return stream.parallel();
    }

    @Override
    public Stream<ConsumerRecord<K, V>> unordered() {
        return stream.unordered();
    }

    @Override
    public Stream<ConsumerRecord<K, V>> onClose(final Runnable closeHandler) {

        return stream.onClose(new Runnable() {

            @Override
            public void run() {
                task.stop();
                closeHandler.run();
            }
        });
    }

    @Override
    public void close() {

        task.stop();
        stream.close();
        isClosed = true;
    }

    public boolean isClosed() {

        return isClosed;
    }

    @Override
    public Stream<ConsumerRecord<K, V>> filter(Predicate<? super ConsumerRecord<K, V>> predicate) {
        return stream.filter(predicate);
    }

    @Override
    public <R> Stream<R> map(Function<? super ConsumerRecord<K, V>, ? extends R> mapper) {
        return stream.map(mapper);
    }

    @Override
    public IntStream mapToInt(ToIntFunction<? super ConsumerRecord<K, V>> mapper) {
        return stream.mapToInt(mapper);
    }

    @Override
    public LongStream mapToLong(ToLongFunction<? super ConsumerRecord<K, V>> mapper) {
        return stream.mapToLong(mapper);
    }

    @Override
    public DoubleStream mapToDouble(ToDoubleFunction<? super ConsumerRecord<K, V>> mapper) {
        return stream.mapToDouble(mapper);
    }

    @Override
    public <R> Stream<R> flatMap(
            Function<? super ConsumerRecord<K, V>, ? extends Stream<? extends R>> mapper) {
        return stream.flatMap(mapper);
    }

    @Override
    public IntStream flatMapToInt(
            Function<? super ConsumerRecord<K, V>, ? extends IntStream> mapper) {
        return stream.flatMapToInt(mapper);
    }

    @Override
    public LongStream flatMapToLong(
            Function<? super ConsumerRecord<K, V>, ? extends LongStream> mapper) {
        return stream.flatMapToLong(mapper);
    }

    @Override
    public DoubleStream flatMapToDouble(
            Function<? super ConsumerRecord<K, V>, ? extends DoubleStream> mapper) {
        return stream.flatMapToDouble(mapper);
    }

    @Override
    public Stream<ConsumerRecord<K, V>> distinct() {
        return stream.distinct();
    }

    @Override
    public Stream<ConsumerRecord<K, V>> sorted() {
        return stream.sorted();
    }

    @Override
    public Stream<ConsumerRecord<K, V>> sorted(Comparator<? super ConsumerRecord<K, V>> comparator) {
        return stream.sorted(comparator);
    }

    @Override
    public Stream<ConsumerRecord<K, V>> peek(Consumer<? super ConsumerRecord<K, V>> action) {
        return stream.peek(action);
    }

    @Override
    public Stream<ConsumerRecord<K, V>> limit(long maxSize) {

        try {
            return stream.limit(maxSize);
        } finally {
            task.stop();
        }
    }

    @Override
    public Stream<ConsumerRecord<K, V>> skip(long n) {
        return stream.skip(n);
    }

    @Override
    public void forEach(Consumer<? super ConsumerRecord<K, V>> action) {

        try {
            stream.forEach(action);
        } finally {
            task.stop();
        }
    }

    @Override
    public void forEachOrdered(Consumer<? super ConsumerRecord<K, V>> action) {

        try {
            stream.forEachOrdered(action);
        } finally {
            task.stop();
        }
    }

    @Override
    public Object[] toArray() {

        try {
            return stream.toArray();
        } finally {
            task.stop();
        }
    }

    @Override
    public <A> A[] toArray(IntFunction<A[]> generator) {

        try {
            return stream.toArray(generator);
        } finally {
            task.stop();
        }
    }

    @Override
    public ConsumerRecord<K, V> reduce(ConsumerRecord<K, V> identity, BinaryOperator<ConsumerRecord<K, V>> accumulator) {

        try {
            return stream.reduce(identity, accumulator);
        } finally {
            task.stop();
        }
    }

    @Override
    public Optional<ConsumerRecord<K, V>> reduce(BinaryOperator<ConsumerRecord<K, V>> accumulator) {

        try {
            return stream.reduce(accumulator);
        } finally {
            task.stop();
        }
    }

    @Override
    public <U> U reduce(U identity, BiFunction<U, ? super ConsumerRecord<K, V>, U> accumulator,
                        BinaryOperator<U> combiner) {

        try {
            return stream.reduce(identity, accumulator, combiner);
        } finally {
            task.stop();
        }
    }

    @Override
    public <R> R collect(Supplier<R> supplier,
                         BiConsumer<R, ? super ConsumerRecord<K, V>> accumulator, BiConsumer<R, R> combiner) {

        try {
            return stream.collect(supplier, accumulator, combiner);
        } finally {
            task.stop();
        }

    }

    @Override
    public <R, A> R collect(Collector<? super ConsumerRecord<K, V>, A, R> collector) {
        try {
            return stream.collect(collector);
        } finally {
            task.stop();
        }
    }

    @Override
    public Optional<ConsumerRecord<K, V>> min(Comparator<? super ConsumerRecord<K, V>> comparator) {

        try {
            return stream.min(comparator);
        } finally {
            task.stop();
        }
    }

    @Override
    public Optional<ConsumerRecord<K, V>> max(Comparator<? super ConsumerRecord<K, V>> comparator) {

        try {
            return stream.max(comparator);
        } finally {
            task.stop();
        }
    }

    @Override
    public long count() {
        try {
            return stream.count();
        } finally {
            task.stop();
        }
    }

    @Override
    public boolean anyMatch(Predicate<? super ConsumerRecord<K, V>> predicate) {
        try {
            return stream.anyMatch(predicate);
        } finally {
            task.stop();
        }
    }

    @Override
    public boolean allMatch(Predicate<? super ConsumerRecord<K, V>> predicate) {

        try {
            return stream.allMatch(predicate);
        } finally {
            task.stop();
        }
    }

    @Override
    public boolean noneMatch(Predicate<? super ConsumerRecord<K, V>> predicate) {

        try {
            return stream.noneMatch(predicate);
        } finally {
            task.stop();
        }
    }

    @Override
    public Optional<ConsumerRecord<K, V>> findFirst() {

        try {
            return stream.findFirst();
        } finally {
            task.stop();
        }
    }

    @Override
    public Optional<ConsumerRecord<K, V>> findAny() {

        try {
            return stream.findAny();
        } finally {
            task.stop();
        }
    }
}