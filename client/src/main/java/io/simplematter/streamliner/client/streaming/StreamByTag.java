/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.simplematter.streamliner.client.streaming;

import io.simplematter.streamliner.client.consumer.ConsumerByTag;
import io.simplematter.streamliner.client.streaming.core.AbstractStream;
import io.simplematter.streamliner.protocol.model.Tag;
import io.simplematter.streamliner.protocol.serialization.Serdes;

import java.util.Properties;
import java.util.concurrent.Executors;

public class StreamByTag<K, V> extends AbstractStream<K, V> {

    public StreamByTag(final Properties props,
                       final Serdes<K> keySerdes,
                       final Serdes<V> valueSerdes,
                       final Tag persistenceTag) throws Exception {

        super(Executors.newFixedThreadPool(10), new ConsumerByTag(props, keySerdes, valueSerdes, persistenceTag));
    }

    public static void main(String[] args) throws Exception {

        /*
        final Properties props = new Properties();
        props.put("io.simplematter.config.server.name", "127.0.0.1");
        props.put("io.simplematter.config.server.port", 1234);
        props.put("io.simplematter.config.topic", "my_persistence");
        props.put("io.simplematter.config.clientId", "client1");
        props.put("io.simplematter.config.username", "stefano");
        props.put("io.simplematter.config.password", "rocco");
        props.put("io.simplematter.config.codec", Codec.class.getName());

        final StreamByTag<String, String> streamById = new StreamByTag<String, String>(props,
                new StringSerdes(),
                new StringSerdes(),
                new Tag("event"));
        final Stream<ConsumerRecord<String, String>> stream = streamById.stream();
        stream.forEach(record -> {
            System.out.println(record);
            //streamById.lookup("", record.getOffset());
        });
        */

        /*
        while (true) {
            final List<ConsumerRecord<String, String>> records = stream.collect(Collectors.toList());
            records.forEach(record -> {
                System.out.println(record);
            });
            if (records.size() > 0 ){
                System.out.println("Aknowledge");
            }
        } */
    }
}
