/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.simplematter.streamliner.client;

import com.ea.async.Async;
import io.simplematter.streamliner.protocol.Offset;
import io.simplematter.streamliner.protocol.messaging.ProducerRecord;
import io.simplematter.streamliner.protocol.messaging.codec.Codec;
import io.simplematter.streamliner.protocol.messaging.request.*;
import io.simplematter.streamliner.protocol.messaging.response.*;
import io.simplematter.streamliner.protocol.model.ClientId;
import io.simplematter.streamliner.protocol.model.Tag;
import io.simplematter.streamliner.protocol.model.Topic;
import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.net.NetSocket;
import io.vertx.core.parsetools.RecordParser;
import io.vertx.core.streams.ReadStream;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class NetworkClient {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final Map<Long, AbstractRequest> requests;
    private final Map<Long, AbstractResponse> responses;
    private final ExecutorService executor;
    private final CircuitBreaker breaker;
    private final Vertx vertx;
    private final Topic topic;
    private final ClientId clientId;
    private final Codec codec;
    private final AtomicLong correlationId;
    private final AtomicBoolean isConnected;
    private final AtomicBoolean isLeader;
    private final NetSocket socket;

    /*
    public static NetworkClient create(final Properties properties) throws Exception {

        return new NetworkClient(properties);
    } */

    private String getString(final Properties properties, final String key) {
        final Object value = properties.get(key);
        if (value instanceof String) {
            Objects.requireNonNull(value);
            return (String) value;
        } else throw new RuntimeException(key + " is not a string");
    }

    private Integer getInteger(final Properties properties, final String key) {
        final Object value = properties.get(key);
        if (value instanceof Integer) {
            Objects.requireNonNull(value);
            return (Integer) value;
        } else throw new RuntimeException(key + " is not a string");
    }

    public NetworkClient(final Properties properties) throws Exception {

        final String configServerName = getString(properties, "streamliner.config.server.name");
        final Integer configServerPort = getInteger(properties, "streamliner.config.server.port");
        final String configTopic = getString(properties, "streamliner.config.topic");
        final String configClientId = getString(properties, "streamliner.config.clientId");
        final String configUsername = getString(properties, "streamliner.config.username");
        final String configPassword = getString(properties, "streamliner.config.password");
        final String configCodec = getString(properties, "streamliner.config.codec");

        final Vertx vertx = Vertx.vertx();
        this.requests = new ConcurrentHashMap<>();
        this.responses = new ConcurrentHashMap<>();
        this.executor = Executors.newFixedThreadPool(1);
        this.breaker = CircuitBreaker.create("client-circuit-breaker", vertx,
                new CircuitBreakerOptions()
                        .setMaxFailures(5) // number of failure before opening the circuit
                        .setTimeout(2000) // consider a failure if the operation does not succeed in time
                        .setFallbackOnFailure(true) // do we call the fallback on failure
                        .setResetTimeout(10000) // time spent in open state before attempting to re-try
        );
        this.vertx = vertx;
        this.codec = (Codec) Class.forName(configCodec).newInstance();
        this.topic = new Topic(configTopic);
        this.clientId = new ClientId(configClientId);
        this.correlationId = new AtomicLong(0);
        this.isLeader = new AtomicBoolean(false);
        this.socket = bind(configServerName, configServerPort);
        // TODO: FIXME (topic)
        this.isConnected = new AtomicBoolean(connect(topic.getValue(), configUsername, configPassword));


    }

    private boolean isConnected() {

        return isConnected.get();
    }

    public boolean isLeader() {

        return isLeader.get();
    }

    private NetSocket bind(final String host, final int port) {

        final CompletableFuture<NetSocket> future = new CompletableFuture<>();
        vertx.createNetClient().connect(port, host, response -> {
            if (response.succeeded()) {
                final NetSocket socket = response.result();

                final RecordParser parser = RecordParser.newFixed(4, (ReadStream<Buffer>) null);
                parser.setOutput(getHandler(parser));
                socket.handler(parser);

                socket.closeHandler(handler -> {
                    logger.info(clientId.getValue() + " is disconnected");
                    close();
                });

                socket.exceptionHandler(exception -> {
                    logger.error(clientId.getValue() + " exception: " + exception.getMessage());
                    close();
                });

                future.complete(socket);
                logger.info(clientId.getValue() + " is connected");
            } else {
                logger.error(clientId.getValue() + " exception: " + response.cause().getMessage());
                future.completeExceptionally(response.cause());
            }
        });
        return Async.await(future);
    }

    private Handler<Buffer> getHandler(final RecordParser parser) {

        return new Handler<Buffer>() {

            int size = -1;

            @Override
            public void handle(Buffer buffer) {
                if (size == -1) {
                    size = buffer.getInt(0);
                    parser.fixedSizeMode(size);
                } else {
                    final Object data = codec.decode(buffer.getBytes());
                    receive(data);
                    //metrics.messageRead(received.address(), buff.length());
                    parser.fixedSizeMode(4);
                    size = -1;
                }
            }
        };
    }

    private void receive(final Object response) {

        logger.info("Response: " + response);
        if (response instanceof ConnectionResponse) {
            responses.put(((ConnectionResponse) response).getHeader().getCorrelationId(), (AbstractResponse) response);
        } else if (response instanceof FetchByKeyResponse) {
            responses.put(((FetchByKeyResponse) response).getHeader().getCorrelationId(), (AbstractResponse) response);
        } else if (response instanceof FetchByTagResponse) {
            responses.put(((FetchByTagResponse) response).getHeader().getCorrelationId(), (AbstractResponse) response);
        } else if (response instanceof FetchByKeyAndTagResponse) {
            responses.put(((FetchByKeyAndTagResponse) response).getHeader().getCorrelationId(), (AbstractResponse) response);
        } else if (response instanceof FetchCursorResponse) {
            responses.put(((FetchCursorResponse) response).getHeader().getCorrelationId(), (AbstractResponse) response);
        } else if (response instanceof FetchMetadataResponse) {
            responses.put(((FetchMetadataResponse) response).getHeader().getCorrelationId(), (AbstractResponse) response);
        } else if (response instanceof ProduceResponse) {
            responses.put(((ProduceResponse) response).getHeader().getCorrelationId(), (AbstractResponse) response);
        } else if (response instanceof CommitCursorResponse) {
            responses.put(((CommitCursorResponse) response).getHeader().getCorrelationId(), (AbstractResponse) response);
        } else if (response instanceof LeadershipNotification) {
            if (((LeadershipNotification) response).isLeader()) {
                logger.info(clientId.getValue() + " is the leader");
            } else {
                logger.info(clientId.getValue() + " is not the leader");
            }
            isLeader.set(((LeadershipNotification) response).isLeader());
        }
    }

    private void write(final AbstractRequest request) {

        breaker.execute(future -> {
            try {
                final Buffer buffer = Buffer.buffer();
                byte[] message = codec.encode(request);
                buffer.appendInt(message.length);
                buffer.appendBytes(message);
                socket.write(buffer);
                future.complete();
            } catch (Exception e) {
                future.fail(e);
            }
        }).setHandler(handler -> {
            if (handler.succeeded()) {
                logger.info(handler.result());
            } else {
                logger.info(handler.cause().getMessage());
            }
        });
    }

    private void send(final AbstractRequest request, final Handler<AbstractResponse> handler) throws IOException {

        logger.info("Request: " + request);
        final CompletableFuture<AbstractResponse> promise = new CompletableFuture<>();
        promise.thenAccept(handler::handle);
        executor.execute(new Task(request, promise));
    }

    private void withConnection(final AbstractRequest request, final Handler<AbstractResponse> handler) throws IOException {

        if (isConnected()) {
            send(request, handler);
        } else {
            throw new IOException("Client is not connected");
        }
    }

    private boolean connect(final String topic, final String username, final String password) {

        final CompletableFuture<ConnectionResponse> promise = new CompletableFuture<>();
        final RequestHeader header = new RequestHeader(0, 1, correlationId.getAndIncrement(),
                clientId.getValue());
        final ConnectionRequest request = new ConnectionRequest(header, topic, username, password);
        try {
            send(request, response -> promise.complete((ConnectionResponse) response));
        } catch (IOException ex) {
            promise.completeExceptionally(ex);
        }
        return Async.await(promise).isSuccessful();
    }

    public FetchCursorResponse fetchCursor() {

        final CompletableFuture<FetchCursorResponse> promise = new CompletableFuture<>();
        final RequestHeader header = new RequestHeader(0, 1, correlationId.getAndIncrement(),
                clientId.getValue());
        final FetchCursorRequest request = new FetchCursorRequest(header, topic.getValue());
        try {
            withConnection(request, response -> promise.complete((FetchCursorResponse) response));
        } catch (IOException ex) {
            promise.completeExceptionally(ex);
        }
        return Async.await(promise);
    }

    public FetchOffsetResponse fetchOffset() {

        final CompletableFuture<FetchOffsetResponse> promise = new CompletableFuture<>();
        final RequestHeader header = new RequestHeader(0, 1,
                correlationId.getAndIncrement(),
                clientId.getValue());
        final FetchOffsetRequest request = new FetchOffsetRequest(header, topic.getValue());
        try {
            withConnection(request, response -> promise.complete((FetchOffsetResponse) response));
        } catch (IOException ex) {
            promise.completeExceptionally(ex);
        }
        return Async.await(promise);
    }

    public ProduceResponse produce(final List<ProducerRecord<byte[], byte[]>> records) {

        final CompletableFuture<ProduceResponse> promise = new CompletableFuture<>();
        final RequestHeader header = new RequestHeader(0, 1, correlationId.getAndIncrement(),
                clientId.getValue());
        final ProduceRequest request = new ProduceRequest(header, topic.getValue(), records);
        try {
            withConnection(request, response -> promise.complete((ProduceResponse) response));
        } catch (IOException ex) {
            promise.completeExceptionally(ex);
        }
        return Async.await(promise);
    }

    public FetchByKeyResponse fetchByKey(final byte[] key, final Offset offset, final int batchSize) {

        final CompletableFuture<FetchByKeyResponse> promise = new CompletableFuture<>();
        final RequestHeader header = new RequestHeader(0, 1, correlationId.getAndIncrement(),
                clientId.getValue());
        // TODO: FIXME
        final FetchByKeyRequest request = new FetchByKeyRequest(header, topic.getValue(), key, offset, batchSize);
        try {
            withConnection(request, response -> promise.complete((FetchByKeyResponse) response));
        } catch (IOException ex) {
            promise.completeExceptionally(ex);
        }
        return Async.await(promise);
    }

    public <K> FetchByKeyResponse fetchByKey(final byte[] key, final Offset offset) {
        return fetchByKey(key, offset, 1000);
    }

    public FetchByTagResponse fetchByTag(final Tag persistenceTag, final Offset offset, final int batchSize) {

        final CompletableFuture<FetchByTagResponse> promise = new CompletableFuture<>();
        final RequestHeader header = new RequestHeader(0, 1, correlationId.getAndIncrement(),
                clientId.getValue());
        final FetchByTagRequest request = new FetchByTagRequest(header, topic.getValue(), persistenceTag.getValue(), offset, batchSize);
        try {
            withConnection(request, response -> promise.complete((FetchByTagResponse) response));
        } catch (IOException ex) {
            promise.completeExceptionally(ex);
        }
        return Async.await(promise);
    }

    public FetchByTagResponse fetchByTag(final Tag persistenceTag, final Offset offset) {
        return fetchByTag(persistenceTag, offset, 1000);
    }

    public FetchByKeyAndTagResponse fetchByKeyAndTag(final byte[] key, final Tag tag, final Offset offset) {

        final CompletableFuture<FetchByKeyAndTagResponse> promise = new CompletableFuture<>();
        final RequestHeader header = new RequestHeader(0, 1, correlationId.getAndIncrement(),
                clientId.getValue());
        final FetchByKeyAndTagRequest request = new FetchByKeyAndTagRequest(header, topic.getValue(), key,
                tag.getValue(), offset);
        try {
            withConnection(request, response -> promise.complete((FetchByKeyAndTagResponse) response));
        } catch (IOException ex) {
            promise.completeExceptionally(ex);
        }
        return Async.await(promise);
    }

    public CommitCursorResponse commitCursor(final Offset offset) {

        final CompletableFuture<CommitCursorResponse> promise = new CompletableFuture<>();
        final RequestHeader header = new RequestHeader(0, 1, correlationId.getAndIncrement(),
                clientId.getValue());
        final CommitCursorRequest request = new CommitCursorRequest(header, topic.getValue(), offset);
        try {
            withConnection(request, response -> promise.complete((CommitCursorResponse) response));
        } catch (IOException ex) {
            promise.completeExceptionally(ex);
        }
        return Async.await(promise);
    }

    public ResetCursorResponse resetCursor(final Offset offset) {

        final CompletableFuture<ResetCursorResponse> promise = new CompletableFuture<>();
        final RequestHeader header = new RequestHeader(0, 1, correlationId.getAndIncrement(),
                clientId.getValue());
        final ResetCursorRequest request = new ResetCursorRequest(header, topic.getValue(), offset);
        try {
            withConnection(request, response -> promise.complete((ResetCursorResponse) response));
        } catch (IOException ex) {
            promise.completeExceptionally(ex);
        }
        return Async.await(promise);
    }

    public void close() {
        isConnected.set(false);
        requests.clear();
        responses.clear();
        if (socket != null) {
            socket.close();
        }
    }

    private class Task implements Runnable {

        private final AbstractRequest request;
        private final CompletableFuture<AbstractResponse> promise;
        private boolean keepRunning;

        private Task(final AbstractRequest request, final CompletableFuture<AbstractResponse> promise) {

            this.request = request;
            this.promise = promise;
            this.keepRunning = true;
        }

        @Override
        public void run() {

            final long correlationId = request.getHeader().getCorrelationId();
            requests.put(correlationId, request);
            write(request);
            while (keepRunning) {
                final AbstractResponse response = responses.get(correlationId);
                if (response != null) {
                    responses.remove(correlationId);
                    requests.remove(correlationId);
                    promise.complete(response);
                    keepRunning = false;
                }
                Thread.yield();
            }
        }
    }
}
