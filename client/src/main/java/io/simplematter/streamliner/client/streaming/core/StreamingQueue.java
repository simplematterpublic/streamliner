package io.simplematter.streamliner.client.streaming.core;

import io.simplematter.streamliner.protocol.messaging.ConsumerRecord;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class StreamingQueue<K, V> implements Consumer<ConsumerRecord<K, V>>, Supplier<ConsumerRecord<K, V>> {

    private final int maxLength;
    private final BlockingQueue<ConsumerRecord<K, V>> queue;

    public StreamingQueue(final int maxLength) {

        this.maxLength = maxLength;
        this.queue = new LinkedBlockingQueue<>(this.maxLength);
    }

    @Override
    public ConsumerRecord<K, V> get() {

        try {
            return queue.take();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    @Override
    public void accept(final ConsumerRecord<K, V> record) {

        try {
            queue.put(record);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    public int maxLength() {
        return this.maxLength;
    }

    public int size() {
        return queue.size();
    }

    public void clear() {
        queue.clear();
    }
}