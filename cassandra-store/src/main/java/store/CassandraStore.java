package store;

import com.datastax.driver.core.*;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import io.simplematter.streamliner.store.Cursor;
import io.simplematter.streamliner.store.IStore;
import io.simplematter.streamliner.protocol.Offset;
import io.simplematter.streamliner.protocol.messaging.ConsumerRecord;
import io.simplematter.streamliner.protocol.messaging.ProducerRecord;
import io.simplematter.streamliner.protocol.model.ClientId;
import io.simplematter.streamliner.protocol.model.Tag;
import io.simplematter.streamliner.protocol.model.Topic;
import io.simplematter.streamliner.store.OffsetCursor;
import io.simplematter.streamliner.store.OffsetStore;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class CassandraStore implements IStore<byte[], byte[]> {

    private static final Logger logger = LoggerFactory.getLogger(CassandraStore.class);
    private final ExecutorService executor;
    private final Session session;
    private final OffsetStore offsetStore;
    private final Topic topic;

    public CassandraStore(final OffsetStore offsetStore, final Session session, final Topic topic) {

        this.executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        this.session = session;
        this.offsetStore = offsetStore;
        this.topic = topic;
        init();
    }

    private void init() {

        // TODO: Hack! Fix me
        try {
            createTopic(getTopic().getValue()).get();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }

        final long partitionNumber = getPartitionNumber();
        // TODO: handle concurrency
        offsetStore.getPartition().set(partitionNumber);
        final long sequenceNumber = getSequenceNumber(partitionNumber);
        // TODO: handle concurrency
        offsetStore.getSequence().set(sequenceNumber);
    }

    public Topic getTopic() {
        return topic;
    }

    public CompletableFuture<Boolean> createTopic(final String name) {

        final CompletableFuture<Boolean> promise = new CompletableFuture<>();

        CompletableFuture.supplyAsync(() -> {
            final String statement1 = "CREATE TABLE " + name + "_journal (" +
                    "key blob," +
                    "tag text," +
                    "partition_number bigint," +
                    "sequence_number bigint," +
                    "timestamp bigint," +
                    "uid text," +
                    "value blob," +
                    "PRIMARY KEY ((key, tag, partition_number), sequence_number)" +
                    ") WITH CLUSTERING ORDER BY (sequence_number ASC);";
            if (logger.isDebugEnabled()) logger.info(statement1);
            session.execute(statement1);

            final String statement2 = "CREATE INDEX " + name + "_journal_idx_1 ON " + name + "_journal (partition_number);";
            if (logger.isDebugEnabled()) logger.info(statement2);
            session.execute(statement2);

            final String statement3 = "CREATE MATERIALIZED VIEW vw_" + name + "_journal_by_tag AS" +
                    " SELECT tag, partition_number, sequence_number, key, timestamp, uid, value" +
                    " FROM " + name + "_journal" +
                    " WHERE tag IS NOT NULL AND partition_number IS NOT NULL AND sequence_number IS NOT NULL AND key IS NOT NULL AND value IS NOT NULL AND uid IS NOT NULL AND timestamp IS NOT NULL" +
                    " PRIMARY KEY ((tag, partition_number), sequence_number, key)" +
                    " WITH CLUSTERING ORDER BY (sequence_number DESC, key DESC);";
            if (logger.isDebugEnabled()) logger.info(statement3);
            session.execute(statement3);

            final String statement4 = "CREATE MATERIALIZED VIEW vw_" + name + "_journal_by_key AS" +
                    " SELECT key, partition_number, sequence_number, tag, timestamp, uid, value" +
                    " FROM " + name + "_journal" +
                    " WHERE key IS NOT NULL AND partition_number IS NOT NULL AND sequence_number IS NOT NULL AND tag IS NOT NULL AND value IS NOT NULL AND uid IS NOT NULL AND timestamp IS NOT NULL" +
                    " PRIMARY KEY ((key, partition_number), sequence_number, tag)" +
                    " WITH CLUSTERING ORDER BY (sequence_number DESC, tag ASC);";
            if (logger.isDebugEnabled()) logger.info(statement4);
            session.execute(statement4);

            final String statement5 = "CREATE TABLE " + name + "_cursor (" +
                    " client_id text PRIMARY KEY,\n" +
                    " partition_number bigint,\n" +
                    " sequence_number bigint\n" +
                    ");";
            if (logger.isDebugEnabled()) logger.info(statement5);
            session.execute(statement5);

            final String statement6 = "CREATE TABLE " + name + "_index (" +
                    " name text," +
                    " value blob," +
                    " partition_number bigint,\n" +
                    " PRIMARY KEY ((name, value), partition_number)" +
                    ") WITH CLUSTERING ORDER BY (partition_number ASC);";
            if (logger.isDebugEnabled()) logger.info(statement6);
            session.execute(statement6);
            return true;
        }, executor).thenAccept(result -> {
            promise.complete(result);
        }).exceptionally(throwable -> {
            promise.completeExceptionally(throwable);
            return null;
        });

        return promise;
    }

    public CompletableFuture<Boolean> dropTopic(final String name) {

        final CompletableFuture<Boolean> promise = new CompletableFuture<>();

        CompletableFuture.supplyAsync(() -> {
            final String statement1 = "DROP TABLE IF EXISTS " + name + "_index";
            if (logger.isDebugEnabled()) logger.info(statement1);
            session.execute(statement1);

            final String statement2 = "DROP TABLE IF EXISTS " + name + "_cursor";
            if (logger.isDebugEnabled()) logger.info(statement2);
            session.execute(statement2);

            final String statement3 = "DROP MATERIALZED VIEW IF EXISTS vw_" + name + "_journal_by_tag";
            if (logger.isDebugEnabled()) logger.info(statement3);
            session.execute(statement3);

            final String statement4 = "DROP MATERIALZED VIEW IF EXISTS vw_" + name + "_journal_by_key";
            if (logger.isDebugEnabled()) logger.info(statement4);
            session.execute(statement4);

            final String statement5 = "DROP INDEX IF EXISTS " + name + "_journal_idx_1";
            if (logger.isDebugEnabled()) logger.info(statement5);
            session.execute(statement5);

            final String statement6 = "DROP TABLE IF EXIXTS " + name + "_journal";
            if (logger.isDebugEnabled()) logger.info(statement6);
            session.execute(statement6);

            return true;
        }, executor).thenAccept(result -> {
            promise.complete(result);
        }).exceptionally(throwable -> {
            promise.completeExceptionally(throwable);
            return null;
        });

        return promise;
    }

    private OffsetCursor getPartition() {

        return offsetStore.getPartition();
    }

    private OffsetCursor getSequence() {

        return offsetStore.getSequence();
    }

    // TODO: Move to zookeper
    private long getPartitionNumber() {

        final String sql = "SELECT MAX(partition_number) FROM " + getTopic().getValue() + "_journal";
        final PreparedStatement preparedStatement = session.prepare(sql);
        final BoundStatement boundStatement = preparedStatement.bind();
        final ResultSet resultSet = session.execute(boundStatement);
        final Row row = resultSet.one();
        return row == null ? 0L : row.getLong(0);
    }

    // TODO: Move to zookeper
    private long getSequenceNumber(final long partitionNumber) {

        final String sql = "SELECT MAX(sequence_number) FROM " + getTopic().getValue() + "_journal WHERE partition_number = ?";
        final PreparedStatement preparedStatement = session.prepare(sql);
        final BoundStatement boundStatement = preparedStatement.bind(partitionNumber);
        final ResultSet resultSet = session.execute(boundStatement);
        final Row row = resultSet.one();
        if (row == null) {
            return -1L;
        } else {
            System.out.println(row.getLong(0));
            if (row.getObject(0) == null) {
                return -1L;
            } else {
                return row.getLong(0);
            }
        }
    }

    @Override
    public CompletableFuture<Offset> write(final ProducerRecord<byte[], byte[]> record) {

        final List<ProducerRecord<byte[], byte[]>> records = new ArrayList<ProducerRecord<byte[], byte[]>>() {
            {
                add(record);
            }
        };
        return write(records);
    }

    @Override
    public CompletableFuture<Offset> write(final List<ProducerRecord<byte[], byte[]>> records) {

        final Cursor offset = new Cursor();
        final CompletableFuture<Offset> promise = new CompletableFuture<>();

        CompletableFuture.supplyAsync(() -> {
            final String sql1 = "INSERT INTO " + getTopic().getValue() + "_index (name, value, partition_number) VALUES (?, ?, ?)";
            final PreparedStatement preparedStatement1 = session.prepare(sql1);

            final String sql2 = "INSERT INTO " + getTopic().getValue() + "_journal (tag, partition_number, sequence_number, key, value, uid, timestamp)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?)";
            final PreparedStatement preparedStatement2 = session.prepare(sql2);

            final BatchStatement batchStatement = new BatchStatement();

            records.forEach(record -> {
                final long partitionNumber = getPartition().get();
                final long sequenceNumber = getSequence().incrementAndGet();

                /*
                final BoundStatement boundStatement1 = preparedStatement1.bind("id",
                        ByteBuffer.wrap(persistenceId.getValue().getBytes()),
                        partitionNumber);
                batchStatement.add(boundStatement1); */
                final BoundStatement boundStatement2 = preparedStatement1.bind("tag",
                        ByteBuffer.wrap(record.getTag().getValue().getBytes()),
                        partitionNumber);
                batchStatement.add(boundStatement2);
                final BoundStatement boundStatement3 = preparedStatement1.bind("key",
                        ByteBuffer.wrap(record.getKey()),
                        partitionNumber);
                batchStatement.add(boundStatement3);
                final BoundStatement boundStatement4 = preparedStatement2.bind(record.getTag().getValue(),
                        partitionNumber,
                        sequenceNumber,
                        ByteBuffer.wrap(record.getKey()),
                        ByteBuffer.wrap(record.getValue()),
                        UUID.randomUUID().toString(),
                        System.currentTimeMillis());
                batchStatement.add(boundStatement4);

                offset.setPartitionNumber(partitionNumber);
                offset.setSequenceNumber(sequenceNumber);
            });

            return session.executeAsync(batchStatement);
        }, executor).thenAccept(future -> {

            Futures.addCallback(future, new FutureCallback<ResultSet>() {

                @Override
                public void onSuccess(final ResultSet resultSet) {

                    promise.complete(offset);
                }

                @Override
                public void onFailure(final Throwable throwable) {
                    promise.completeExceptionally(throwable);

                }

            }, executor);
        });

        return promise;
    }

    @Override
    public CompletableFuture<Boolean> acknowledge(final ClientId clientId, final Offset offset) {

        final CompletableFuture<Boolean> promise = new CompletableFuture<>();

        CompletableFuture.supplyAsync(() -> {
            final Cursor offsetStore = (Cursor) offset;
            final String sql = "INSERT INTO " + getTopic().getValue() + "_cursor (client_id, partition_number, sequence_number) VALUES (?, ?, ?);";
            final PreparedStatement preparedStatement = session.prepare(sql);
            final BoundStatement boundStatement = preparedStatement.bind(clientId.getValue(), offsetStore.getPartitionNumber(),
                    offsetStore.getSequenceNumber());

            return session.executeAsync(boundStatement);
        }, executor).thenAccept(future -> {

            Futures.addCallback(future, new FutureCallback<ResultSet>() {

                @Override
                public void onSuccess(final ResultSet resultSet) {
                    promise.complete(true);
                }

                @Override
                public void onFailure(final Throwable throwable) {
                    promise.completeExceptionally(throwable);
                }

            }, executor);
        });

        return promise;
    }

    @Override
    public CompletableFuture<Boolean> reset(final ClientId clientId, final Offset offset) {

        return acknowledge(clientId, offset);
    }

                /*
                QueryBuilder queryBuilder = QueryBuilder.select("partitionNumber")
                        .from("my_keyspace_2", "my_persistence_index_by_id").where() */

    /*
    @Override
    public CompletableFuture<List<ConsumerRecord<byte[], byte[]>>> byId(final PersistenceId persistenceId, final Offset offset,
                                                                        final int batchSize) {

        final CompletableFuture<List<ConsumerRecord<byte[], byte[]>>> promise = new CompletableFuture<>();

        fetchById(persistenceId, offset, batchSize).thenAccept(records -> {
            if (records.size() == 0) {
                fetchIndex("id", persistenceId.getValue().getBytes()).thenAccept(futureOffset -> {
                    final Cursor currentOffset = (Cursor) offset;
                    if (futureOffset.compareTo(currentOffset) > 0) {
                        fetchById(persistenceId, futureOffset, batchSize).thenAccept(results -> {
                            promise.complete(results);
                        });
                    } else {
                        promise.complete(new ArrayList<>());
                    }
                });

            } else {
                promise.complete(records);
            }
        });

        return promise;
    }

    private CompletableFuture<List<ConsumerRecord<byte[], byte[]>>> fetchById(final PersistenceId persistenceId,
                                                                              final Offset offset, final int batchSize) {

        final CompletableFuture<List<ConsumerRecord<byte[], byte[]>>> promise = new CompletableFuture<>();

        CompletableFuture.supplyAsync(() -> {
            final Cursor offsetStore = (Cursor) offset;
            final String sql = "SELECT persistence_id, persistence_tag, partition_number, sequence_number, uid, key, k_manifest, value, v_manifest, timestamp FROM vw_" + getTopic().getValue() + "_journal_by_id"
                    + " WHERE persistence_id = ? AND partition_number = ? AND sequence_number > ? ORDER BY sequence_number ASC LIMIT " + batchSize;
            final PreparedStatement preparedStatement = session.prepare(sql);
            final BoundStatement boundStatement = preparedStatement.bind(persistenceId.getValue(),
                    offsetStore.getPartitionNumber(),
                    offsetStore.getSequenceNumber());
            return session.executeAsync(boundStatement);
        }, executor).thenAccept(future -> {

            Futures.addCallback(future, new FutureCallback<ResultSet>() {

                @Override
                public void onSuccess(final ResultSet resultSet) {
                    final List<ConsumerRecord<byte[], byte[]>> records = resultSet.all().stream().map(row -> toRecord(row)).collect(Collectors.toList());
                    promise.complete(records);
                }

                @Override
                public void onFailure(final Throwable throwable) {
                    // TODO: Log
                    promise.complete(new ArrayList<>());
                }

            }, executor);
        });

        return promise;
    } */

    @Override
    public CompletableFuture<List<ConsumerRecord<byte[], byte[]>>> byKey(final byte[] key, final Offset offset, final int batchSize) {

        final CompletableFuture<List<ConsumerRecord<byte[], byte[]>>> promise = new CompletableFuture<>();

        fetchByKey(key, offset, batchSize).thenAccept(records -> {
            if (records.size() == 0) {
                fetchIndex("key", key).thenAccept(futureOffset -> {
                    final Cursor currentOffset = (Cursor) offset;
                    if (futureOffset.compareTo(currentOffset) > 0) {
                        fetchByKey(key, futureOffset, batchSize).thenAccept(results -> {
                            promise.complete(results);
                        });
                    } else {
                        promise.complete(new ArrayList<>());
                    }
                });
            } else {
                promise.complete(records);
            }
        });

        return promise;
    }

    private CompletableFuture<List<ConsumerRecord<byte[], byte[]>>> fetchByKey(final byte[] key, final Offset offset, final int batchSize) {

        final CompletableFuture<List<ConsumerRecord<byte[], byte[]>>> promise = new CompletableFuture<>();

        CompletableFuture.supplyAsync(() -> {
            final Cursor offsetStore = (Cursor) offset;
            final String sql = "SELECT tag, partition_number, sequence_number, key, value, uid, timestamp FROM vw_" + getTopic().getValue() + "_journal_by_key"
                    + " WHERE key = ? AND partition_number = ? AND sequence_number > ? ORDER BY sequence_number ASC LIMIT " + batchSize;
            ;
            final PreparedStatement preparedStatement = session.prepare(sql);
            final BoundStatement boundStatement = preparedStatement.bind(ByteBuffer.wrap(key),
                    offsetStore.getPartitionNumber() < 0 ? 0L : offsetStore.getPartitionNumber(),
                    offsetStore.getSequenceNumber());
            return session.executeAsync(boundStatement);
        }, executor).thenAccept(resultSet -> {

            Futures.addCallback(resultSet, new FutureCallback<ResultSet>() {

                @Override
                public void onSuccess(final ResultSet resultSet) {
                    final List<ConsumerRecord<byte[], byte[]>> records = resultSet.all().stream()
                            .map(row -> toRecord(row)).filter(r -> Arrays.equals(r.getKey(), key))
                            .collect(Collectors.toList());
                    promise.complete(records);
                }

                @Override
                public void onFailure(final Throwable t) {
                    // TODO: Log error
                    promise.complete(new ArrayList<>());
                }

            }, executor);
        });

        return promise;
    }

    @Override
    public CompletableFuture<List<ConsumerRecord<byte[], byte[]>>> byKeyAndTag(final byte[] key, final Tag tag, final Offset offset) {

        final CompletableFuture<List<ConsumerRecord<byte[], byte[]>>> promise = new CompletableFuture<>();

        fetchByKeyAndTag(key, tag, offset).thenAccept(records -> {
            if (records.size() == 0) {
                fetchIndex("key", key).thenAccept(futureOffset -> {
                    final Cursor currentOffset = (Cursor) offset;
                    if (futureOffset.compareTo(currentOffset) > 0) {
                        fetchByKeyAndTag(key, tag, futureOffset).thenAccept(results -> {
                            promise.complete(results);
                        });
                    } else {
                        promise.complete(new ArrayList<>());
                    }
                });
            } else {
                promise.complete(records);
            }
        });

        return promise;
    }

    private CompletableFuture<List<ConsumerRecord<byte[], byte[]>>> fetchByKeyAndTag(final byte[] key, final Tag tag,
                                                                                     final Offset offset) {

        final CompletableFuture<List<ConsumerRecord<byte[], byte[]>>> promise = new CompletableFuture<>();

        CompletableFuture.supplyAsync(() -> {
            final Cursor offsetStore = (Cursor) offset;
            final String sql = "SELECT tag, partition_number, sequence_number, key, value, uid, timestamp FROM vw_" + getTopic().getValue() + "_journal_by_key"
                    + " WHERE key = ? AND partition_number = ? AND sequence_number >= ? ORDER BY sequence_number ASC";
            final PreparedStatement preparedStatement = session.prepare(sql);
            final BoundStatement boundStatement = preparedStatement.bind(ByteBuffer.wrap(key),
                    offsetStore.getPartitionNumber() < 0 ? 0L : offsetStore.getPartitionNumber(),
                    offsetStore.getSequenceNumber());
            return session.executeAsync(boundStatement);
        }, executor).thenAccept(resultSet -> {

            Futures.addCallback(resultSet, new FutureCallback<ResultSet>() {

                @Override
                public void onSuccess(final ResultSet resultSet) {
                    final List<ConsumerRecord<byte[], byte[]>> records = resultSet.all().stream()
                            .map(row -> toRecord(row)).filter(r -> Arrays.equals(r.getKey(), key))
                            .collect(Collectors.toList());
                    promise.complete(records);
                }

                @Override
                public void onFailure(final Throwable t) {
                    // TODO: Log error
                    promise.complete(new ArrayList<>());
                }

            }, executor);
        });

        return promise;
    }

    @Override
    public CompletableFuture<List<ConsumerRecord<byte[], byte[]>>> byTag(final Tag tag,
                                                                         final Offset offset, final int batchSize) {

        final CompletableFuture<List<ConsumerRecord<byte[], byte[]>>> promise = new CompletableFuture<>();

        fetchByTag(tag, offset, batchSize).thenAccept(records -> {
            if (records.size() == 0) {
                fetchIndex("tag", tag.getValue().getBytes()).thenAccept(futureOffset -> {
                    final Cursor currentOffset = (Cursor) offset;
                    if (futureOffset.compareTo(currentOffset) > 0) {
                        fetchByTag(tag, futureOffset, batchSize).thenAccept(results -> {
                            promise.complete(results);
                        });
                    } else {
                        promise.complete(new ArrayList<>());
                    }
                });
            } else {
                promise.complete(records);
            }
        });

        return promise;
    }

    private CompletableFuture<List<ConsumerRecord<byte[], byte[]>>> fetchByTag(final Tag tag,
                                                                               final Offset offset, final int batchSize) {

        final CompletableFuture<List<ConsumerRecord<byte[], byte[]>>> promise = new CompletableFuture<>();

        CompletableFuture.supplyAsync(() -> {
            final Cursor offsetStore = (Cursor) offset;
            final String sql = "SELECT tag, partition_number, sequence_number, key, value, uid, timestamp FROM vw_" + getTopic().getValue() + "_journal_by_tag"
                    + " WHERE tag = ? AND partition_number = ? AND sequence_number > ? ORDER BY sequence_number ASC LIMIT " + batchSize;
            final PreparedStatement preparedStatement = session.prepare(sql);
            final BoundStatement boundStatement = preparedStatement.bind(tag.getValue(),
                    offsetStore.getPartitionNumber(),
                    offsetStore.getSequenceNumber());
            return session.executeAsync(boundStatement);
        }, executor).thenAccept(future -> {

            Futures.addCallback(future, new FutureCallback<ResultSet>() {

                @Override
                public void onSuccess(final ResultSet resultSet) {
                    final List<ConsumerRecord<byte[], byte[]>> records = resultSet.all().stream().map(row -> toRecord(row)).collect(Collectors.toList());
                    promise.complete(records);
                }

                @Override
                public void onFailure(final Throwable throwable) {
                    promise.complete(new ArrayList<>());
                }

            }, executor);
        });

        return promise;
    }

    @Override
    public CompletableFuture<List<ConsumerRecord<byte[], byte[]>>> byTagAndKey(final Tag tag,
                                                                               final byte[] key, final Offset offset) {

        final CompletableFuture<List<ConsumerRecord<byte[], byte[]>>> promise = new CompletableFuture<>();

        fetchByTagAndKey(tag, key, offset).thenAccept(records -> {
            if (records.size() == 0) {
                fetchIndex("tag", tag.getValue().getBytes()).thenAccept(futureOffset -> {
                    final Cursor currentOffset = (Cursor) offset;
                    if (futureOffset.compareTo(currentOffset) > 0) {
                        fetchByTagAndKey(tag, key, futureOffset).thenAccept(results -> promise.complete(results));
                    } else {
                        promise.complete(new ArrayList<>());
                    }
                });
            } else {
                promise.complete(records);
            }
        });

        return promise;
    }

    private CompletableFuture<List<ConsumerRecord<byte[], byte[]>>> fetchByTagAndKey(final Tag tag,
                                                                                     final byte[] key, final Offset offset) {

        final CompletableFuture<List<ConsumerRecord<byte[], byte[]>>> promise = new CompletableFuture<>();

        CompletableFuture.supplyAsync(() -> {
            final Cursor offsetStore = (Cursor) offset;
            final String sql = "SELECT tag, partition_number, sequence_number, key, value, uid, timestamp FROM vw_" + getTopic().getValue() + "_journal_by_tag"
                    + " WHERE tag = ? AND partition_number = ? AND sequence_number >= ? ORDER BY sequence_number ASC";
            final PreparedStatement preparedStatement = session.prepare(sql);
            final BoundStatement boundStatement = preparedStatement.bind(tag.getValue(),
                    offsetStore.getPartitionNumber(),
                    offsetStore.getSequenceNumber());
            return session.executeAsync(boundStatement);
        }, executor).thenAccept(resultSet -> {

            Futures.addCallback(resultSet, new FutureCallback<ResultSet>() {

                @Override
                public void onSuccess(final ResultSet resultSet) {
                    final List<ConsumerRecord<byte[], byte[]>> records = resultSet.all().stream()
                            .map(row -> toRecord(row)).filter(r -> Arrays.equals(r.getKey(), key))
                            .collect(Collectors.toList());
                    promise.complete(records);
                }

                @Override
                public void onFailure(final Throwable throwable) {
                    // TODO: Log error
                    promise.complete(new ArrayList<>());
                }

            }, executor);
        });

        return promise;
    }

    @Override
    public CompletableFuture<Offset> getOffset(final ClientId clientId) {

        final CompletableFuture<Offset> promise = new CompletableFuture<>();

        CompletableFuture.supplyAsync(() -> {
            final CompletableFuture<Offset> future = new CompletableFuture<>();
            final String sql = "SELECT partition_number, sequence_number FROM " + getTopic().getValue() + "_cursor" +
                    " WHERE client_id = ? LIMIT 1";
            final PreparedStatement preparedStatement = session.prepare(sql);
            final BoundStatement boundStatement = preparedStatement.bind(clientId.getValue());
            return session.executeAsync(boundStatement);
        }, executor).thenAccept(future -> {
            Futures.addCallback(future, new FutureCallback<ResultSet>() {

                @Override
                public void onSuccess(final ResultSet resultSet) {
                    final Row row = resultSet.one();
                    if (row == null) {
                        promise.complete(new Cursor());
                    } else {
                        promise.complete(new Cursor(row.getLong(0), row.getLong(1)));
                    }
                }

                @Override
                public void onFailure(final Throwable throwable) {
                    promise.completeExceptionally(throwable);
                }

            }, executor);
        });

        return promise;
    }

    // TODO: FIXME
    @Override
    public CompletableFuture<Offset> getOffset() {

        return CompletableFuture.supplyAsync(() -> {
            long partitionNumber = getPartitionNumber();
            long sequenceNumber = getSequenceNumber(partitionNumber);
            return new Cursor(partitionNumber, sequenceNumber);
        });
    }

    private CompletableFuture<Offset> fetchIndex(final String name, final byte[] value) {

        final CompletableFuture<Offset> promise = new CompletableFuture<>();

        CompletableFuture.supplyAsync(() -> {
            final String sql = "SELECT partition_number FROM " + getTopic().getValue() + "_index WHERE name = ? AND value = ? LIMIT 1";
            final PreparedStatement preparedStatement = session.prepare(sql);
            final BoundStatement boundStatement = preparedStatement.bind(name, ByteBuffer.wrap(value));
            return session.executeAsync(boundStatement);
        }, executor).thenAccept(future -> {

            Futures.addCallback(future, new FutureCallback<ResultSet>() {

                @Override
                public void onSuccess(final ResultSet resultSet) {

                    final Row row = resultSet.one();
                    if (row != null) {
                        promise.complete(new Cursor(row.getLong(0), -1));

                    } else {
                        promise.complete(new Cursor());
                    }
                }

                @Override
                public void onFailure(final Throwable throwable) {
                    promise.completeExceptionally(throwable);
                }

            }, executor);
        });

        return promise;
    }

    private ConsumerRecord<byte[], byte[]> toRecord(final Row row) {

        return new ConsumerRecord<byte[], byte[]>(row.getString(5),
                new Tag(row.getString(0)),
                row.getBytes(3).array(),
                row.getBytes(4).array(),
                new Cursor(row.getLong(1), row.getLong(2)),
                row.getLong(6));
    }

    /*
    private Offset getOffset(final PersistenceId persistenceId) {

        final long partitionNumber = getPartitionNumber(persistenceId);
        final long sequenceNumber = getSequenceNumber(persistenceId, partitionNumber);
        return new OffsetStore(partitionNumber, sequenceNumber);
    }

    // TODO: Move to zookeper
    private long getPartitionNumber(final PersistenceId persistenceId) {

        String sql = "SELECT FROM my_persistence_index WHERE address = ?";
        final PreparedStatement preparedStatement = session.prepare(sql);
        final BoundStatement boundStatement = preparedStatement.bind(persistenceId.getValue());
        final ResultSet resultSet = session.execute(boundStatement);
        final Row row = resultSet.one();
        return row == null ? 0L : row.getLong(0);
    }

    // TODO: Move to zookeper
    private long getSequenceNumber(final PersistenceId persistenceId, long partitionNumber) {

        String sql = "SELECT MAX(sequence_number) FROM my_persistence_journal WHERE partition_number = ?";
        final PreparedStatement preparedStatement = session.prepare(sql);
        final BoundStatement boundStatement = preparedStatement.bind(partitionNumber);
        final ResultSet resultSet = session.execute(boundStatement);
        final Row row = resultSet.one();
        return row == null ? 0L : row.getLong(0);
    } */

}
