package store;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class ChuckedStorage {

    final ExecutorService executor;
    private byte[] buffer;

    public ChuckedStorage(final ExecutorService executor, final byte[] buffer) {

        this.executor = executor;
        this.buffer = buffer;
    }

    public List<byte[]> split(byte[] buffer, int length) {

        final List<byte[]> bytes = new ArrayList<>();
        final int buckets = (buffer.length / length) + 1;
        for (int i = 0; i < buckets; i++) {
            int from = i * length;
            int to = Math.min((i + 1) * length, buffer.length);
            bytes.add(Arrays.copyOfRange(buffer, from, to));
        }
        return bytes;
    }

    public byte[] merge(List<byte[]> bytes) throws IOException {

        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bytes.forEach(item -> {
            try {
                outputStream.write(item);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        outputStream.close();
        return outputStream.toByteArray();
    }

    public void write() {

        final AtomicInteger counter = new AtomicInteger();
        split(buffer, 1024).forEach(item -> {
            //System.out.println(item.length);
            executor.execute(new Writer(counter.getAndIncrement(), item, buffer.length));
        });
    }

    private class Writer implements Runnable {

        private int bucket;
        private byte[] buffer;
        private int length;

        public Writer(final int bucket, final byte[] buffer, int length) {

            this.bucket = bucket;
            this.buffer = buffer;
            this.length = length;
        }

        @Override
        public void run() {


            String statement = "INSERT INTO my_persistence_journal (key, tag, partition_number, sequence_number, segment_number, timestamp, uid, value) VALUES ";
            statement += "(key, tag, 0, 0, " + bucket + ", " + System.currentTimeMillis() + ", " + UUID.randomUUID().toString() + ", value)";
            System.out.println(statement);

            /*
            int from = ((bucket * 1024));
            int to = (((bucket * 1024)) + buffer.length);
            int l = buffer.length;

            System.out.println("Inserting " + bucket + " " + from + " " + to + " " + length + " " + new String(buffer)); */

        }
    }

    public static void main(String[] args) throws Exception {

        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < 2000; i++) {
            buffer.append("A" + i + ";");
        }

        ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        ChuckedStorage storage = new ChuckedStorage(executor, buffer.toString().getBytes());
        storage.write();

        /*
        ChuckedStorage storage = new ChuckedStorage(buffer.toString().getBytes());
        storage.split(buffer.toString().getBytes(), 1024).forEach(bytes -> {
            System.out.println("*********************************");
            System.out.println(new String(bytes));
        });

        System.out.println(new String(storage.merge(storage.split(buffer.toString().getBytes(), 1024))));
        */

    }
}
