package store;

import com.datastax.driver.core.Session;
import com.ea.async.Async;
import io.simplematter.streamliner.protocol.model.Topic;
import io.simplematter.streamliner.store.*;
import io.vertx.core.Vertx;

import java.util.concurrent.CompletableFuture;

public class CassandraStoreFactory implements IStoreFactory {

    private final Vertx vertx;
    private final Session session;

    public CassandraStoreFactory(final Session session) {

        this.vertx = Vertx.vertx();

        this.session = session;
    }

    @Override
    public <K, V> IStore<K, V> getStore(final Topic topic) {

        final CompletableFuture<IStore<K, V>> promise = CompletableFuture.supplyAsync(() -> {
            final OffsetCursor partitionCounter = new AtomicCounter(vertx, topic.getValue() + "-partition");
            final OffsetCursor sequenceCounter = new AtomicCounter(vertx, topic.getValue() + "-sequence");
            return (IStore<K, V>) new CassandraStore(new OffsetStore(partitionCounter, sequenceCounter), session, topic);
        });
        return Async.await(promise);
    }

}
