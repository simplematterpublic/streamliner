package store;

import com.datastax.driver.core.*;
import io.simplematter.streamliner.protocol.Offset;
import io.simplematter.streamliner.store.Cursor;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class NewStorage {

    final ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    private Session session;

    public NewStorage(final Session session) {

        this.session = session;
    }

    public List<byte[]> split(byte[] buffer, int length) {

        final List<byte[]> bytes = new ArrayList<>();
        final int buckets = (buffer.length / length) + 1;
        for (int i = 0; i < buckets; i++) {
            int from = i * length;
            int to = Math.min((i + 1) * length, buffer.length);
            bytes.add(Arrays.copyOfRange(buffer, from, to));
        }
        return bytes;
    }

    public byte[] merge(List<byte[]> bytes) {

        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bytes.forEach(item -> {
            try {
                outputStream.write(item);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        try {
            outputStream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return outputStream.toByteArray();
    }

    public CompletableFuture<Offset> byKey(byte[] key) {

        final String statement = "SELECT key, tag, par_num, seq_num, seg_num, timestamp, uid, value" +
                " FROM vw_my_persistence_journal_test_by_key WHERE key = ? AND par_num = ? AND seq_num > ?" +
                " ORDER BY seq_num ASC";
        final PreparedStatement preparedStatement = session.prepare(statement);
        final BoundStatement boundStatement = preparedStatement.bind(ByteBuffer.wrap(key), 0L, -1L);
        final ResultSet resultSet = session.execute(boundStatement);
        final List<byte[]> bytes = new ArrayList<>();
        final AtomicInteger counter = new AtomicInteger();
        resultSet.all().forEach(row -> {
            System.out.println(counter.getAndIncrement() + " " + row.getLong(4));
            bytes.add(row.getBytes(7).array());
        });
        Collections.reverse(bytes);
        System.out.println(new String(merge(bytes)));
        return null;
    }

    public CompletableFuture<Offset> write(byte[] key, String tag, byte[] value) {

        final Cursor offset = new Cursor();

        final CompletableFuture<Offset> promise = new CompletableFuture<>();

        final AtomicLong counter = new AtomicLong();
        final String statement = "INSERT INTO my_persistence_journal_test (key, tag, par_num, seq_num, seg_num, timestamp, uid, value) VALUES" +
                " (?, ?, ?, ?, ?, ?, ?, ?)";
        final BatchStatement batchStatement = new BatchStatement();
        split(value, 1024).forEach(chunk -> {
            System.out.println(statement);
            final PreparedStatement preparedStatement = session.prepare(statement);
            final BoundStatement boundStatement = preparedStatement.bind(ByteBuffer.wrap(key),
                    tag, 0L, 0L, counter.getAndIncrement(), System.currentTimeMillis(), UUID.randomUUID().toString(), ByteBuffer.wrap(chunk));
            session.execute(boundStatement);
            //batchStatement.add(boundStatement);
        });
        //session.execute(batchStatement);

        /*
        CompletableFuture.supplyAsync(() -> {
            final String statement = "INSERT INTO my_persistence_journal_test (key, tag, par_num, seq_num, seg_num, timestamp, uid, value) VALUES" +
                    " (?, ?, ?, ?, ?, ?, ?, ?)";
            final BatchStatement batchStatement = new BatchStatement();
            split(value, 1024).forEach(chunk -> {
                System.out.println(statement);
                final PreparedStatement preparedStatement = session.prepare(statement);
                final BoundStatement boundStatement = preparedStatement.bind(ByteBuffer.wrap(key),
                        tag, 0, 0, 0, System.currentTimeMillis(), UUID.randomUUID().toString(), value);
                batchStatement.add(boundStatement);
            });
            return session.executeAsync(batchStatement);
        }, executor).thenAccept(future -> {

            Futures.addCallback(future, new FutureCallback<ResultSet>() {

                @Override
                public void onSuccess(final ResultSet resultSet) {

                    System.out.println("Yes");
                    promise.complete(offset);
                }

                @Override
                public void onFailure(final Throwable throwable) {

                    System.out.println("No");
                    promise.completeExceptionally(throwable);
                }

            }, executor);
        });  */

        return promise;
    }

    public static void main(String[] args) throws Exception {

        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < 10000; i++) {
            buffer.append("A" + i + ";");
        }

        final Cluster cluster = Cluster.builder().addContactPoint("127.0.0.1").build();
        final Session session = cluster.connect("my_keyspace_3");
        final NewStorage storage = new NewStorage(session);
        //CompletableFuture<Offset> cursor = storage.write("mykey".getBytes(), "mytag", buffer.toString().getBytes());
        //System.out.println(cursor.get());
        storage.byKey("mykey".getBytes());
    }
}
