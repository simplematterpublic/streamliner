package io.simplematter.streamliner;

public class WaitFor<T> {

    private Boolean isReady = false;
    private T value;

    public WaitFor() {

    }

    public void set(final T value) {

        synchronized (this) {
            this.value = value;
            this.isReady = true;
            notifyAll();
        }
    }

    public T get(final long timeout) {

        synchronized (this) {
            while(!isReady) {
                try {
                    wait(timeout);
                    isReady = false;
                } catch (InterruptedException ex) {
                    isReady = false;
                }
            }
        }
        return value;
    }


    public static void main(String[] args) {

        final WaitFor<String> waitFor = new WaitFor<>();

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                    System.out.println("Unlocking");
                    waitFor.set("Hello");
                } catch (InterruptedException ex) {

                }
            }
        }).start();

        System.out.println(waitFor.get(3000));
    }
}
