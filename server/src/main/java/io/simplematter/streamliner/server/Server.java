package io.simplematter.streamliner.server;

import io.simplematter.streamliner.protocol.messaging.codec.Codec;
import io.simplematter.streamliner.store.IStoreFactory;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.spi.cluster.ClusterManager;

public class Server extends AbstractVerticle {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final ClusterManager clusterManager;
    private final IStoreFactory storeFactory;
    private final String host;
    private final int port;

    public Server(final ClusterManager clusterManager, final IStoreFactory storeFactory, String host, int port) {

        this.clusterManager = clusterManager;
        this.storeFactory = storeFactory;
        this.host = host;
        this.port = port;
    }

    @Override
    public void start() throws Exception {

        logger.info("Starting Server");
        final VertxOptions options = new VertxOptions().setClusterManager(clusterManager);

        Vertx.clusteredVertx(options, res -> {
            if (res.succeeded()) {
                vertx = res.result();
                final AbstractVerticle verticle = new Broker(storeFactory, new Codec(), host, port);
                vertx.deployVerticle(verticle, deployResponse -> {
                    if (deployResponse.failed()) {
                        logger.error("Unable to deploy verticle " + verticle.getClass().getSimpleName(),
                                deployResponse.cause());
                    } else {
                        logger.info("Verticle " + verticle.getClass().getSimpleName() + " deployed");
                    }
                });
                logger.info("Server started");
            } else {
                logger.error("Server not started");
                logger.error(res.cause().getMessage());
            }
        });
    }
}
