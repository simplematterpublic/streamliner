package io.simplematter.streamliner.server;

import com.ea.async.Async;
import io.simplematter.streamliner.protocol.Offset;
import io.simplematter.streamliner.protocol.messaging.ConsumerRecord;
import io.simplematter.streamliner.protocol.messaging.codec.Codec;
import io.simplematter.streamliner.protocol.messaging.request.*;
import io.simplematter.streamliner.protocol.messaging.response.*;
import io.simplematter.streamliner.protocol.model.ClientId;
import io.simplematter.streamliner.protocol.model.Tag;
import io.simplematter.streamliner.protocol.model.Topic;
import io.simplematter.streamliner.store.IStore;
import io.simplematter.streamliner.store.IStoreFactory;
import io.vertx.core.*;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.net.NetServer;
import io.vertx.core.net.NetSocket;
import io.vertx.core.parsetools.RecordParser;
import io.vertx.core.shareddata.Lock;
import io.vertx.core.streams.ReadStream;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

public class Broker extends AbstractVerticle {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final Map<NetSocket, InternalClient> sockets;
    private final Map<String, IStore<byte[], byte[]>> stores;
    private final IStoreFactory storeFactory;
    private final Codec codec;
    private final String host;
    private final int port;

    public Broker(final IStoreFactory storeFactory, final Codec codec, final String host, final int port) {

        this.sockets = new ConcurrentHashMap<>();
        this.stores = new ConcurrentHashMap<>();
        this.storeFactory = storeFactory;
        this.codec = codec;
        this.host = host;
        this.port = port;
    }

    private final IStore<byte[], byte[]> getStore(final Topic topic) {

        IStore<byte[], byte[]> store = stores.get(topic.getValue());
        if (store == null) {
            final CompletableFuture<IStore<byte[], byte[]>> promise = CompletableFuture.supplyAsync(() -> {
                //final AtomicCounter partition = new AtomicCounter(vertx, topic.getValue() + ".partition");
                //final AtomicCounter sequence = new AtomicCounter(vertx, topic.getValue() + ".sequence");
                final IStore<byte[], byte[]> newStore = storeFactory.getStore(topic);
                stores.put(topic.getValue(), newStore);
                return newStore;
            });
            store = Async.await(promise);
        }
        return store;
    }

    @Override
    public void start() throws Exception {

        logger.info("Starting Broker");
        for (int i = 0; i < Runtime.getRuntime().availableProcessors(); i++) {
            final NetServer server = vertx.createNetServer(
                    //new NetServerOptions().setPort(1234).setHost("localhost")
            );
            final int processor = i + 1;
            server.connectHandler(socket -> {

                final RecordParser parser = RecordParser.newFixed(4, (ReadStream<Buffer>) null);

                final Handler<Buffer> handler = new Handler<Buffer>() {

                    int size = -1;

                    @Override
                    public void handle(Buffer buffer) {
                        if (size == -1) {
                            size = buffer.getInt(0);
                            parser.fixedSizeMode(size);
                        } else {
                            final AbstractRequest data = (AbstractRequest) codec.decode(buffer.getBytes());
                            receive(socket, data);
                            //metrics.messageRead(received.address(), buff.length());
                            parser.fixedSizeMode(4);
                            size = -1;
                        }
                    }
                };

                parser.setOutput(handler);

                socket.handler(parser);

                socket.closeHandler(buffer -> {

                    logger.info("Disposing client");
                    final InternalClient client = sockets.get(socket);
                    if (client != null) {
                        client.close();
                        logger.info("Client removed");
                        sockets.remove(socket);
                    } else {
                        logger.info("Client does not exist");
                    }
                    logger.info("Client disposed");
                });

                socket.exceptionHandler(exception -> exception.printStackTrace());
                logger.info("Starting Started");

            });
            server.listen(port, host, handler -> {
                if (handler.succeeded()) {
                    logger.info("Broker (" + processor + ") listening on " + host + ":" + port);
                } else {
                    logger.error(handler.cause());
                }
            });
        }
    }

    private void receive(final NetSocket socket, final AbstractRequest request) {

        if (logger.isDebugEnabled()) logger.debug("Receiving " + request);
        if (request instanceof ConnectionRequest) {
            logger.info("Connecting " + request.getClientId() + " -> " + socket.remoteAddress());
            InternalClient client = sockets.get(socket);
            if (client == null) {
                logger.info("Client not found. Creating client " + request.getClientId());
                client = new InternalClient(socket, new ClientId(request.getClientId()));
                sockets.put(socket, client);
                client.handle(request);
            } else {
                if (client.isClosed()) {
                    logger.info("Client is closed. Creating client " + request.getClientId());
                    client = new InternalClient(socket, new ClientId(request.getClientId()));
                    sockets.put(socket, client);
                    client.handle(request);
                } else {
                    if (client.getClientId().equals(new ClientId(request.getClientId()))) {
                        logger.info("Client " + request.getClientId() + " exists");
                        client.handle(request);
                    } else {
                        logger.warn("Client mismatch: " + client.getClientId() + " vs " + request.getClientId());
                        logger.warn("Closing client " + request.getClientId());
                        socket.close();
                        // client.handle(request);
                    }
                }
            }
        } else {
            InternalClient client = sockets.get(socket);
            if (client != null) {
                if (client.getClientId().equals(new ClientId(request.getClientId()))) {
                    logger.info("Client " + request.getClientId() + " exists");
                    client.handle(request);
                } else {
                    logger.warn("Client mismatch: " + client.getClientId() + " vs " + request.getClientId());
                    logger.warn("Closing client " + request.getClientId());
                    socket.close();
                }
            } else {
                logger.warn("Client " + request.getClientId() + " does not exist");
                logger.warn("Closing client " + request.getClientId());
                socket.close();
            }
        }
    }

    private void send(final NetSocket socket, final AbstractResponse response) {

        if (logger.isDebugEnabled()) logger.debug("Sending " + response);
        final byte[] message = codec.encode(response);
        final Buffer buffer = Buffer.buffer()
                .appendInt(message.length)
                .appendBytes(message);
        socket.write(buffer);
    }

    private void notifyLeadership(final NetSocket socket, final boolean isLeader) {

        final ResponseHeader header = new ResponseHeader(-1);
        final AbstractResponse response = new LeadershipNotification(header, isLeader);
        send(socket, response);
    }

    private class InternalClient {

        private final NetSocket socket;
        private final ClientId clientId;
        private Lock lock;
        private boolean isLeader = false;
        private boolean isClosed = false;

        private InternalClient(final NetSocket socket, final ClientId clientId) {

            this.socket = socket;
            this.clientId = clientId;
        }

        public NetSocket getSocket() {

            return socket;
        }

        public ClientId getClientId() {

            return clientId;
        }

        private Lock getLock() {

            return lock;
        }

        public boolean isLeader() {
            return isLeader;
        }

        public boolean isClosed() {
            return isClosed;
        }

        private void acquire(final Vertx vertx) {

            logger.info("Acquiring lock for client " + getClientId().getValue());
            vertx.sharedData().getLock(getClientId().getValue(), result -> {
                if (result.succeeded()) {
                    lock = result.result();
                    isLeader = true;
                    notifyLeadership(getSocket(), isLeader());
                    logger.info("Lock acquired for client " + getClientId().getValue());
                    logger.info("Client " + getClientId().getValue() + " is now leader");
                } else {
                    vertx.setTimer(1000, res -> acquire(vertx));
                }
            });
        }

        private void handle(final AbstractRequest request) {

            if (request instanceof ConnectionRequest) {
                final ConnectionRequest command = (ConnectionRequest) request;
                final ResponseHeader header = new ResponseHeader(command.getCorrelationId());
                final ConnectionResponse response = new ConnectionResponse(header, true);
                send(getSocket(), response);
                acquire(vertx);
            } else if (request instanceof FetchByKeyRequest) {
                final FetchByKeyRequest command = (FetchByKeyRequest) request;
                if (isLeader()) {
                    final Topic topic = new Topic(command.getTopic());
                    getStore(topic).byKey(command.getKey(), command.getOffset(), command.getBatchSize()).thenAccept(result -> {
                        final ResponseHeader header = new ResponseHeader(command.getCorrelationId());
                        final List<ConsumerRecord<byte[], byte[]>> records = (List<ConsumerRecord<byte[], byte[]>>) result;
                        final FetchByKeyResponse response = new FetchByKeyResponse(header, records);
                        send(getSocket(), response);
                    });
                } else {
                    logger.warn("Client " + request.getClientId() + " is not leader");
                    final ResponseHeader header = new ResponseHeader(command.getCorrelationId());
                    final FetchByKeyResponse response = new FetchByKeyResponse(header, new ArrayList<>());
                    send(getSocket(), response);
                }
            } else if (request instanceof FetchByTagRequest) {
                final FetchByTagRequest command = (FetchByTagRequest) request;
                if (isLeader()) {
                    final Topic topic = new Topic(command.getTopic());
                    getStore(topic).byTag(new Tag(command.getTag()), command.getOffset(), command.getBatchSize()).thenAccept(result -> {
                        final ResponseHeader header = new ResponseHeader(command.getHeader().getCorrelationId());
                        final FetchByTagResponse response = new FetchByTagResponse(header, result);
                        send(getSocket(), response);
                    });
                } else {
                    logger.warn("Client " + request.getClientId() + " is not leader");
                    final ResponseHeader header = new ResponseHeader(command.getHeader().getCorrelationId());
                    final FetchByTagResponse response = new FetchByTagResponse(header, new ArrayList<>());
                    send(getSocket(), response);
                }
            } else if (request instanceof FetchByKeyAndTagRequest) {
                final FetchByKeyAndTagRequest command = (FetchByKeyAndTagRequest) request;
                getStore(new Topic(command.getTopic())).byKeyAndTag(command.getKey(), new Tag(command.getTag()), command.getOffset()).thenAccept(result -> {
                    final ResponseHeader header = new ResponseHeader(command.getHeader().getCorrelationId());
                    final FetchByKeyAndTagResponse response = new FetchByKeyAndTagResponse(header, result);
                    send(getSocket(), response);
                });
            } else if (request instanceof FetchCursorRequest) {
                final FetchCursorRequest command = (FetchCursorRequest) request;
                final Topic topic = new Topic(command.getTopic());
                getStore(topic).getOffset(new ClientId(command.getHeader().getClientId()))
                        .thenAccept(result -> {
                            final ResponseHeader header = new ResponseHeader(command.getHeader().getCorrelationId());
                            final FetchCursorResponse response = new FetchCursorResponse(header, (Offset) result);
                            send(getSocket(), response);
                        });
            } else if (request instanceof FetchOffsetRequest) {
                final FetchOffsetRequest command = (FetchOffsetRequest) request;
                final Topic topic = new Topic(command.getTopic());
                getStore(topic).getOffset().thenAccept(result -> {
                    final ResponseHeader header = new ResponseHeader(command.getHeader().getCorrelationId());
                    final FetchOffsetResponse response = new FetchOffsetResponse(header, (Offset) result);
                    send(getSocket(), response);
                });
            } else if (request instanceof FetchMetadataRequest) {
                final FetchMetadataRequest command = (FetchMetadataRequest) request;
                final ResponseHeader header = new ResponseHeader(command.getHeader().getCorrelationId());
                final FetchMetadataResponse response = new FetchMetadataResponse(header);
                send(getSocket(), response);
            } else if (request instanceof ProduceRequest) {
                final ProduceRequest command = (ProduceRequest) request;
                final Topic topic = new Topic(command.getTopic());
                getStore(topic).write(command.getRecords())
                        .thenAccept(result -> {
                            final ResponseHeader header = new ResponseHeader(command.getHeader().getCorrelationId());
                            final ProduceResponse response = new ProduceResponse(header, (Offset) result);
                            send(getSocket(), response);
                        });
            } else if (request instanceof CommitCursorRequest) {
                final CommitCursorRequest command = (CommitCursorRequest) request;
                if (isLeader()) {
                    final Topic topic = new Topic(command.getTopic());
                    getStore(topic).acknowledge(new ClientId(command.getClientId()), command.getOffset())
                            .thenAccept(result -> {
                                final ResponseHeader header = new ResponseHeader(command.getHeader().getCorrelationId());
                                final CommitCursorResponse response = new CommitCursorResponse(header, (Boolean) result);
                                send(getSocket(), response);
                            });
                } else {
                    logger.warn("Client " + request.getClientId() + " is not leader");
                    final ResponseHeader header = new ResponseHeader(command.getHeader().getCorrelationId());
                    final CommitCursorResponse response = new CommitCursorResponse(header, false);
                    send(getSocket(), response);
                }
            } else if (request instanceof ResetCursorRequest) {
                final ResetCursorRequest command = (ResetCursorRequest) request;
                if (isLeader()) {
                    final Topic topic = new Topic(command.getTopic());
                    getStore(topic).reset(new ClientId(command.getClientId()), command.getOffset())
                            .thenAccept(result -> {
                                final ResponseHeader header = new ResponseHeader(command.getHeader().getCorrelationId());
                                final ResetCursorResponse response = new ResetCursorResponse(header, (Boolean) result);
                                send(getSocket(), response);
                            });
                } else {
                    logger.warn("Client " + request.getClientId() + " is not leader");
                    final ResponseHeader header = new ResponseHeader(command.getHeader().getCorrelationId());
                    final ResetCursorResponse response = new ResetCursorResponse(header, false);
                    send(getSocket(), response);
                }
            } else {
                logger.warn("Invalid request from client " + request.getClientId());
                close();
            }
        }

        public void close() {

            logger.info("Closing client " + getClientId());
            if (getLock() != null) {
                vertx.executeBlocking(future -> {
                            logger.info("Releasing lock for client " + getClientId().getValue());
                            getLock().release();
                            future.complete();

                        }, false,
                        handler -> logger.info("Lock released for client " + getClientId().getValue()));
            }
            if (!isClosed()) {
                if (getSocket() != null) {
                    notifyLeadership(getSocket(), false);
                    getSocket().close();
                }
            }
            isLeader = false;
            isClosed = true;
            logger.info("Client " + getClientId().getValue() + " closed");
        }
    }
}
