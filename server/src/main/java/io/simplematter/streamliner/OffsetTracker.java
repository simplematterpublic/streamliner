package io.simplematter.streamliner;

import io.simplematter.streamliner.client.consumer.core.IConsumer;
import io.simplematter.streamliner.protocol.Offset;
import io.simplematter.streamliner.store.Cursor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OffsetTracker<K, V> {

    private final ExecutorService executor;
    private final List<OffsetWrapper> offsets;
    private final IConsumer<K, V> consumer;

    public OffsetTracker(final ExecutorService executor, final IConsumer<K, V> consumer) {

        this.offsets = Collections.synchronizedList(new ArrayList<OffsetWrapper>());
        this.executor = executor;
        this.consumer = consumer;
        this.executor.submit(new Committer());
    }

    public void add(final Offset offset) {

        final OffsetWrapper wrapper = new OffsetWrapper(offset, false);
        System.out.println("Adding " + offsets.size() + " -> " + wrapper);
        offsets.add(wrapper);
        //executor.submit(new Add(offset));
    }

    public void remove(final Offset offset) {

        executor.submit(new Remove(offset));
    }

    private class OffsetWrapper {

        private final Offset offset;
        private final boolean processed;

        private OffsetWrapper(final Offset offset, final boolean processed) {

            this.offset = offset;
            this.processed = processed;
        }

        public OffsetWrapper processed() {
            return new OffsetWrapper(offset, true);
        }

        public String toString() {

            return "{offset:" + offset + ",processed:" + processed + "}";
        }
    }

    private class Add implements Runnable {

        private final Offset offset;

        public Add(final Offset offset) {

            this.offset = offset;
        }

        public void run() {

            final OffsetWrapper wrapper = new OffsetWrapper(offset, false);
            System.out.println("Adding " + offsets.size() + " -> " + wrapper);
            offsets.add(wrapper);
        }
    }

    private class Remove implements Runnable {

        private final Offset offset;

        public Remove(final Offset offset) {

            this.offset = offset;
        }

        public void run() {

            for (int i = 0; i < offsets.size(); i++) {
                final OffsetWrapper wrapper = offsets.get(i);
                if (wrapper.offset.compareTo(offset) == 0) {
                    System.out.println("Removing " + i + " -> " + wrapper.processed());
                    offsets.set(i, wrapper.processed());
                }
            }
        }

    }

    private class Committer implements Runnable {

        public void run() {

            while (true) {
                final List<OffsetWrapper> processed = new ArrayList<>();
                for (int i = 0; i < offsets.size(); i++) {
                    final OffsetWrapper wrapper = offsets.get(i);
                    if (wrapper.processed) {
                        processed.add(wrapper);
                    } else {
                        break;
                    }
                }
                if (processed.size() > 0) {
                    final OffsetWrapper wrapper = processed.get(processed.size() - 1);
                    System.out.println("Acknowledging " + wrapper);
                    /*
                    if (consumer.acknowledge(wrapper.offset)) {
                        for (int i = 0; i < processed.size(); i++) {
                            offsets.remove(0);
                        }
                    } */
                }
                Thread.yield();
            }
        }
    }

    public static void main(String[] args) {

        final OffsetTracker tracker = new OffsetTracker(Executors.newFixedThreadPool(10), null);
        for (int i = 0; i < 1000; i++) {
            tracker.add(new Cursor(0, i));
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {

            }
            tracker.remove(new Cursor(0, i));
        }
    }
}
