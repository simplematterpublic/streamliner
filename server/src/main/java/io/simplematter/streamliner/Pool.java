package io.simplematter.streamliner;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Pool {

    private static Map<String, ExecutorService> executors;

    static {
        executors = Collections.synchronizedMap(new HashMap<>());
    }

    public static ExecutorService getExecutor(final String name) {

        ExecutorService executor = executors.get(name);
        if (executor == null) {
            executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
            executors.put(name, executor);
        }
        return executor;
    }
}
