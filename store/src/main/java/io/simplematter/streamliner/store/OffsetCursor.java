package io.simplematter.streamliner.store;

public interface OffsetCursor {

    boolean set(final long value);

    long get();

    long getAndIncrement();

    long incrementAndGet();

}