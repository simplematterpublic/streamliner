package io.simplematter.streamliner.store;

import io.simplematter.streamliner.protocol.model.Topic;

public interface IStoreFactory {

    <K, V> IStore<K, V> getStore(Topic topic);
}
