package io.simplematter.streamliner.store;

import com.ea.async.Async;
import io.vertx.core.Vertx;
import io.vertx.core.shareddata.SharedData;

import java.util.concurrent.CompletableFuture;

public class AtomicCounter implements OffsetCursor {

    private final String name;
    private final SharedData sharedData;

    public AtomicCounter(final Vertx vertx, final String name) {

        this.name = name;
        this.sharedData = vertx.sharedData();
    }

    public String getName() {
        return name;
    }

    public boolean set(final long value) {

        System.out.println("Setting counter " + getName() + " to " + value);
        final CompletableFuture<Boolean> promise = new CompletableFuture<>();
        sharedData.getCounter(getName(), response -> {
            if (response.succeeded()) {
                response.result().compareAndSet(0L, value, counter -> {
                    if (counter.succeeded()) {
                        promise.complete(counter.result());
                    } else {
                        promise.complete(false);
                    }
                });
            } else {
                promise.completeExceptionally(response.cause());
            }
        });
        return Async.await(promise);
    }

    public long get() {

        System.out.println("Getting counter " + getName());
        final CompletableFuture<Long> promise = new CompletableFuture<>();
        sharedData.getCounter(getName(), response -> {
            if (response.succeeded()) {
                response.result().get(counter -> {
                    if (counter.succeeded()) {
                        promise.complete(counter.result());
                    } else {
                        promise.completeExceptionally(counter.cause());
                    }
                });
            } else {
                promise.completeExceptionally(response.cause());
            }
        });
        return Async.await(promise);
    }

    public long getAndIncrement() {

        System.out.println("Incrementing counter " + getName());
        final CompletableFuture<Long> promise = new CompletableFuture<>();
        sharedData.getCounter(getName(), response -> {
            if (response.succeeded()) {
                response.result().getAndIncrement(counter -> {
                    if (counter.succeeded()) {
                        promise.complete(counter.result());
                    } else {
                        promise.completeExceptionally(counter.cause());
                    }
                });
            } else {
                promise.completeExceptionally(response.cause());
            }
        });
        return Async.await(promise);
    }

    public long incrementAndGet() {

        System.out.println("Incrementing counter " + getName());
        final CompletableFuture<Long> promise = new CompletableFuture<>();
        sharedData.getCounter(getName(), response -> {
            if (response.succeeded()) {
                response.result().incrementAndGet(counter -> {
                    if (counter.succeeded()) {
                        promise.complete(counter.result());
                    } else {
                        promise.completeExceptionally(counter.cause());
                    }
                });
            } else {
                promise.completeExceptionally(response.cause());
            }
        });
        return Async.await(promise);
    }
}
