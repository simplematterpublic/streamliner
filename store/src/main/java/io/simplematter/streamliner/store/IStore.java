package io.simplematter.streamliner.store;

import io.simplematter.streamliner.protocol.Offset;
import io.simplematter.streamliner.protocol.messaging.ConsumerRecord;
import io.simplematter.streamliner.protocol.messaging.ProducerRecord;
import io.simplematter.streamliner.protocol.model.ClientId;
import io.simplematter.streamliner.protocol.model.Tag;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface IStore<K, V> {

    CompletableFuture<Offset> write(final ProducerRecord<K, V> record);

    CompletableFuture<Offset> write(final List<ProducerRecord<K, V>> records);

    CompletableFuture<Boolean> acknowledge(final ClientId clientId, final Offset offset);

    CompletableFuture<Boolean> reset(final ClientId clientId, final Offset offset);

    //CompletableFuture<List<ConsumerRecord<K, V>>> byId(final Offset offset, final int batchSize);

    CompletableFuture<List<ConsumerRecord<K, V>>> byKey(final K key, final Offset offset, final int batchSize);

    CompletableFuture<List<ConsumerRecord<K, V>>> byTag(final Tag tag, final Offset offset, final int batchSize);

    CompletableFuture<List<ConsumerRecord<K, V>>> byKeyAndTag(final K key, final Tag tag, final Offset offset);

    CompletableFuture<List<ConsumerRecord<K, V>>> byTagAndKey(final Tag tag, final K key, final Offset offset);

    //CompletableFuture<List<ConsumerRecord<K, V>>> byKey(final K key, final Offset offset);

    CompletableFuture<Offset> getOffset(final ClientId clientId);

    CompletableFuture<Offset> getOffset();
}
