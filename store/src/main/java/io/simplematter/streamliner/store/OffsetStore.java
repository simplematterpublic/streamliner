package io.simplematter.streamliner.store;

public class OffsetStore {

    private final OffsetCursor partition;
    private final OffsetCursor sequence;

    public OffsetStore(final OffsetCursor partition, final OffsetCursor sequence) {

        this.partition = partition;
        this.sequence = sequence;
    }

    public OffsetCursor getPartition() {

        return partition;
    }

    public OffsetCursor getSequence() {

        return sequence;
    }
}
