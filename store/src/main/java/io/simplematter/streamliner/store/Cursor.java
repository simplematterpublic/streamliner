package io.simplematter.streamliner.store;

import io.simplematter.streamliner.protocol.Offset;

import java.util.concurrent.atomic.AtomicLong;

public class Cursor implements Offset {

    private final AtomicLong partitionNumber;
    private final AtomicLong sequenceNumber;

    public Cursor(long partitionNumber, long sequenceNumber) {

        this.partitionNumber = new AtomicLong(partitionNumber);
        this.sequenceNumber = new AtomicLong(sequenceNumber);
    }

    public Cursor() {

        this(-1, -1);
    }

    public void setPartitionNumber(final long partitionNumber) {

        this.partitionNumber.set(partitionNumber);
    }

    public void setSequenceNumber(final long sequenceNumber) {

        this.sequenceNumber.set(sequenceNumber);
    }

    public long getPartitionNumber() {

        return partitionNumber.get();
    }

    public long getSequenceNumber() {

        return sequenceNumber.get();
    }

    @Override
    public int compareTo(final Offset offset) {
        if (offset == null) {
            return 1;
        } else {
            if (getPartitionNumber() > ((Cursor) offset).getPartitionNumber()) {
                return 1;
            } else if (getPartitionNumber() == ((Cursor) offset).getPartitionNumber()) {
                if (getSequenceNumber() > ((Cursor) offset).getSequenceNumber()) {
                    return 1;
                } else if (getSequenceNumber() < ((Cursor) offset).getSequenceNumber()) {
                    return -1;
                } else {
                    return 0;
                }
            } else {
                return -1;
            }
        }
    }

    @Override
    public String toString() {

        final StringBuilder builder = new StringBuilder();
        builder.append("{partitionNumber:");
        builder.append(getPartitionNumber());
        builder.append(",sequenceNumber:");
        builder.append(getSequenceNumber());
        builder.append("}");
        return builder.toString();
    }
}